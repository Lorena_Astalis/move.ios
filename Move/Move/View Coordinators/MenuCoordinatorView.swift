//
//  MenuCoordinatorView.swift
//  Move
//
//  Created by Lorena Astalis on 07.05.2021.
//

import SwiftUI
import NavigationStack

struct MenuCoordinatorView: View {
    @State var username = ""
    @State var email = ""
    let navigationViewModel: NavigationStack
    
    var body: some View {
        MenuView(onBack: {
            navigationViewModel.pop()
        }, onSeeHistory: handleHistory,
        onAccount: { username, email in
            self.username = username
            self.email = email
            handleAccount()
        }, onChangePassword: handleChangePassword)
        .onReceive(Session.shared.objectWillChange, perform: { _ in
                    navigationViewModel.pop(to: .root)
        })
    }
    
    func handleHistory() {
        navigationViewModel.push(HistoryView(onBack: {
            navigationViewModel.pop()
        }))
    }
    
    func handleAccount() {
        navigationViewModel.push(
            AccountView(
                username: $username,
                email: $email,
                onBack: {
                    navigationViewModel.pop()
                }, onLogout: {
                    navigationViewModel.pop(to: .root)
                })
        )
    }
    
    func handleChangePassword() {
        navigationViewModel.push(ChangePasswordView(onBack: {
            navigationViewModel.pop()
        }))
    }
}
