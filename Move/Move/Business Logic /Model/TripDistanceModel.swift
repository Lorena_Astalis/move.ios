//
//  TripDistanceModel.swift
//  Move
//
//  Created by Lorena Astalis on 13.05.2021.
//

import Foundation

class TripDistance: Codable {
    var totalDistance: Double
    
    enum CodingKeys: String, CodingKey {
        case totalDistance = "totalDistance"
    }
}
