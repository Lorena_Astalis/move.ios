//
//  AccountView.swift
//  Move
//
//  Created by Lorena Astalis on 23.04.2021.
//

import SwiftUI

struct AccountView: View {
    @State var isFocused = [true, true]
    @State var waiting = false
    @Binding var username: String
    @Binding var email: String
    let onBack: () -> Void
    let onLogout: () -> Void
    
    var body: some View {
        VStack {
            StickyHeaderView(backImage: Image("arrow-left-glyph"), title: "Account", image: nil, color: .indigo, onBack: {
                onBack()
            })
            .padding(.bottom, 24)
            VStack {
                CustomTextFieldView(id: 0, text: $email, image: nil, placeholder: "Email", isSecured: false, isFocused: $isFocused, secure: false, underComment: nil, isVisible: true, error: false, errorMessage: nil, color: Color.cloudy)
                    .padding(.bottom, 12)
                CustomTextFieldView(id: 0, text: $username, image: nil, placeholder: "Username", isSecured: false, isFocused: $isFocused, secure: false, underComment: nil, isVisible: true, error: false, errorMessage: nil, color: Color.cloudy)
                    .padding(.bottom, 12)
            }
            .padding(.horizontal, 24)
            Spacer()
            Button {
                waiting = true
                API.logout()
                onLogout()
            } label: {
                HStack {
                    if !waiting {
                        Image("logout-glyph")
                    } else {
                        ProgressView()
                            .progressViewStyle(CircularProgressViewStyle(tint: Color.cloudy))
                    }
                    Text("Log out")
                        .font(.baiJamjureeMedium14)
                        .foregroundColor(.trueRed)
                }
            }
            .padding(.bottom, 24)
            ButtonView(text: "Save edits", activateButton: false, callback: {
                
            }, image: nil, color: .cloudy, waiting: waiting)
            .padding(24)
        }
    }
}

struct AccountView_Previews: PreviewProvider {
    static var previews: some View {
        AccountView(username: .constant("Lorena"), email: .constant("lorena@email.com"),onBack: {}, onLogout: {})
    }
}
