//
//  RegisterUser.swift
//  Move
//
//  Created by Lorena Astalis on 12.04.2021.
//

import Alamofire
import SwiftUI
import UIKit

struct APIError: Error {
    let message: String
    var localizedDescription: String {
        return message
    }
}

typealias Result<T> = Swift.Result<T, Error>

struct API {
    static let baseURL = "https://move-tapp.herokuapp.com/api/"
    
    static func handleResponse<T: Decodable>(response: AFDataResponse<Data?>) -> Result<T> {
        do {
            if response.response?.statusCode == 401 {
                Session.shared.authToken = nil
                Session.shared.shouldInvalidateSession = true
                return .failure(APIError.init(message: "Session not valid"))
            }
            if response.response?.statusCode == 200 {
                let result = try JSONDecoder().decode(T.self, from: response.data!)
                return .success(result)
            } else {
                if let data = response.data {
                    let result = try JSONDecoder().decode(Message.self, from: data)
                    return .failure(APIError(message: result.message))
                } else {
                    return .failure(APIError(message: "No data found"))
                }
            }
        } catch DecodingError.keyNotFound(let key, let context) {
            print("could not find key \(key) in JSON: \(context.debugDescription)")
        } catch DecodingError.valueNotFound(let type, let context) {
            print("could not find type \(type) in JSON: \(context.debugDescription)")
        } catch DecodingError.typeMismatch(let type, let context) {
            print("type mismatch for type \(type) in JSON: \(context.debugDescription)")
        } catch DecodingError.dataCorrupted(let context) {
            print("data found to be corrupted in JSON: \(context.debugDescription)")
        } catch let error as NSError {
            NSLog("Error in read(from:ofType:) domain= \(error.domain), description= \(error.localizedDescription)")
        }
        return .failure(APIError(message: "no data"))
    }
    
    static func register(username: String, email: String, password: String, _ callback: @escaping (Result<Auth>) -> Void) {
        let params = ["username": username,
                      "email": email,
                      "password": password]
        let path = baseURL + "auth/register/"
        AF.request(path, method: .post, parameters: params, encoder: JSONParameterEncoder.default).response {
            response in
            debugPrint(response)
            let result: Result<Auth> = handleResponse(response: response)
            callback(result)
        }
    }
    
    static func login(email: String, password: String, _ callback: @escaping (Result<Auth>) -> Void) {
        let params = ["email": email,
                      "password": password]
        let path = baseURL + "auth/login/"
        AF.request(path, method: .post, parameters: params, encoder: JSONParameterEncoder.default).response {
            response in
                let result: Result<Auth> = handleResponse(response: response)
                callback(result)
        }
    }
    
    static func getScooters(long: Double, lat: Double, _ callback: @escaping (Result<ScooterResult>) -> Void) {
        if let token = Session.shared.authToken {
            let path = baseURL + "scooters"
            let params: [String: Any] = ["lat": lat, "long": long]
            let header: HTTPHeaders = ["Authorization": "Bearer " + token]
            AF.request(path, method: .get, parameters: params, encoding: URLEncoding.queryString, headers: header).response { response in
                let result: Result<ScooterResult> = handleResponse(response: response)
                callback(result)
            }
        }
    }
    
    static func uploadDriverLicense(image: Image, _ callback: @escaping (Result<Bool>) -> Void) {
        guard let token = Session.shared.authToken else {
            callback(.failure(APIError.init(message: "Invalid token")))
            return
        }
        let path = baseURL + "user/upload"
        let header: HTTPHeaders = ["Authorization": "Bearer " + token]
        let uiImage = image.asUIImage()
        let imageData = uiImage.jpegData(compressionQuality: 0.85)
        AF.upload(multipartFormData: {
            multipartFormData in
            if let data = imageData {
                multipartFormData.append(data, withName: "file", fileName: "test.jpg", mimeType: "image/jpg")
            }
        }, to: path, usingThreshold: UInt64.init(), method: .post, headers: header, interceptor: nil, fileManager: FileManager.default, requestModifier: nil).response { response in
            debugPrint(response)
            if response.response?.statusCode == 200 {
                callback(.success(true))
            } else {
                callback(.failure(APIError.init(message: "Failed to upload")))
            }
        }
    }
    
    static func logout() {
        guard let token = Session.shared.authToken else {
            return
        }
        let path = baseURL + "auth/logout/"
        let header: HTTPHeaders = ["Authorization": "Bearer " + token]
        AF.request(path, method: .delete, parameters: ["":""], encoder: JSONParameterEncoder.default, headers: header, interceptor: nil, requestModifier: nil).response { response in
            if let data = response.data {
                let jsonString = String(data: data, encoding: .utf8)
                print(jsonString ?? "No data")
                Session.shared.authToken = nil
            }
        }
        Session.shared.authToken = nil
    }
    
    static func codeUnlockScooter(location: [Double], code: String, address: String, _ callback: @escaping (Result<Booking>) -> Void) {
        guard let token = Session.shared.authToken else {
            callback(.failure(APIError(message: "Not valid session")))
            return
        }
        let path = baseURL + "bookings?lat=\(location[0])&long=\(location[1])/"
        let params = ["scooterNumber": code, "address": address]
        let header: HTTPHeaders = ["Authorization": "Bearer " + token]
        
        AF.request(path, method: .post, parameters: params, encoder: JSONParameterEncoder.default, headers: header, interceptor: nil, requestModifier: nil).response { response in
            debugPrint(response)
            let result: Result<Booking> = handleResponse(response: response)
            callback(result)
        }
    }
    
    static func qrUnlockScooter(code: String, address: String, _ callback: @escaping (Result<Booking>) -> Void) {
        guard let token = Session.shared.authToken else {
            callback(.failure(APIError(message: "Not valid session")))
            return
        }
        let path = baseURL + "bookings?fromQr=true"
        let params = ["scooterNumber": code, "address": address]
        let header: HTTPHeaders = ["Authorization": "Bearer " + token]
        
        AF.request(path, method: .post, parameters: params, encoder: JSONParameterEncoder.default, headers: header, interceptor: nil, requestModifier: nil).response { response in
            debugPrint(response)
            let result: Result<Booking> = handleResponse(response: response)
            callback(result)
        }
    }
    
    static func nfcUnlockScooter(code: String, address: String, _ callback: @escaping (Result<Booking>) -> Void) {
        guard let token = Session.shared.authToken else {
            callback(.failure(APIError(message: "Not valid session")))
            return
        }
        let path = baseURL + "bookings?fromNfc=true"
        let params = ["scooterNumber": code, "address": address]
        let header: HTTPHeaders = ["Authorization": "Bearer " + token]
        
        AF.request(path, method: .post, parameters: params, encoder: JSONParameterEncoder.default, headers: header, interceptor: nil, requestModifier: nil).response { response in
            debugPrint(response)
            let result: Result<Booking> = handleResponse(response: response)
            callback(result)
        }
    }
    
    static func getCurrentBooking(_ callback: @escaping (Result<Booking>) -> Void) {
        guard let token = Session.shared.authToken else {
            callback(.failure(APIError(message: "Not valid session")))
            return
        }
        let path = baseURL + "bookings/ongoing"
        let header: HTTPHeaders = ["Authorization": "Bearer " + token]
        AF.request(path, method: .get, headers: header).response { response in
            debugPrint(response)
            let result: Result<Booking> = handleResponse(response: response)
            callback(result)
        }
    }
    
    static func lockUnlockScooter(bookingId: String, _ callback: @escaping (Result<LockUnlockResponse>) -> Void) {
        guard let token = Session.shared.authToken else {
            callback(.failure(APIError.init(message: "Invalid token")))
            return
        }
        let path = API.baseURL + "bookings/\(bookingId)/lock"
        let header: HTTPHeaders = ["Authorization": "Bearer " + token]
        
        AF.request(path, method: .put, encoding: URLEncoding.httpBody, headers: header ).response {
            response in
            debugPrint(response)
            let result: Result<LockUnlockResponse> = handleResponse(response: response)
            callback(result)
        }
    }
    
    static func endRide(long: Double, lat: Double, address: String, _ callback: @escaping (Result<BookingComplete>) -> Void) {
        guard let token = Session.shared.authToken else {
            callback(.failure(APIError.init(message: "Invalid token")))
            return
        }
        let path = API.baseURL + "bookings/end?lat=\(lat)&long=\(long)"
        let header: HTTPHeaders = ["Authorization": "Bearer " + token]
        let params = ["address":address]
        
        AF.request(path, method: .put, parameters: params, encoding: JSONEncoding.default, headers: header).response {
            response in
            debugPrint(response)
            let result: Result<BookingComplete> = handleResponse(response: response)
            callback(result)
        }
    }
    
    static func updateTripData(long: Double, lat: Double, callback: @escaping (Result<TripDistance>) -> Void) {
        guard let token = Session.shared.authToken else {
            callback(.failure(APIError.init(message: "Invalid token")))
            return
        }
        let path = baseURL + "bookings/ongoing?lat=\(lat)&long=\(long)"
        let header: HTTPHeaders = ["Authorization" : "Bearer " + token]
        
        AF.request(path, method: .put, headers: header).response { response in
            let result: Result<TripDistance> = handleResponse(response: response)
            callback(result)
        }
    }
    
    static func pingScooter(scooterId: String, lat: Double, long: Double, callback: @escaping (Result<PingScooter>) -> Void) {
        guard let token = Session.shared.authToken else {
            callback(.failure(APIError.init(message: "Invalid token")))
            return
        }
        print("Want to ping: lat = \(lat) long = \(long)")
        let path = baseURL + "scooters/ping/\(scooterId)/?lat=\(lat)&long=\(long)"
        let header: HTTPHeaders = ["Authorization" : "Bearer " + token]
            AF.request(path, method: .get, headers: header).response { response in
                debugPrint(response)
                let result: Result<PingScooter> = handleResponse(response: response)
                callback(result)
        }
    }
    
    static func getAllBookings(start: Int, length: Int, _ callback: @escaping (Result<AllBookings>) -> Void) {
        guard let token = Session.shared.authToken else {
            callback(.failure(APIError.init(message: "Invalid token")))
            return
        }
        let path = baseURL + "bookings/?start=\(start)&length=\(length)"
        let header: HTTPHeaders = ["Authorization" : "Bearer " + token]
        
        AF.request(path, method: .get, headers: header).response { response in
            debugPrint(response)
            let result: Result<AllBookings> = handleResponse(response: response)
            callback(result)
        }
    }
    
    static func getCurrentUser(_ callback: @escaping (Result<User>) -> Void) {
        guard let token = Session.shared.authToken else { return }
        let path = baseURL + "user/me"
        let header: HTTPHeaders = ["Authorization" : "Bearer " + token]
        
        AF.request(path, method: .get, headers: header).response { response in
            debugPrint(response)
            let result: Result<User> = handleResponse(response: response)
            callback(result)
        }
    }
    
    static func getScooterById(scooterId: String, _ callback: @escaping (Result<ScooterResponse>) -> Void) {
        guard let token = Session.shared.authToken else { return }
        let path = baseURL + "scooters/\(scooterId)"
        let header: HTTPHeaders = ["Authorization" : "Bearer " + token]
        
        AF.request(path, method: .get, headers: header).response { response in
            debugPrint(response)
            let result: Result<ScooterResponse> = handleResponse(response: response)
            callback(result)
        }
    }
    
    static func changePassword(oldPassword: String, newPassword: String, _ callback: @escaping (Result<Password>) -> Void) {
        guard let token = Session.shared.authToken else {
            callback(.failure(APIError.init(message: "Invalid token")))
            return
        }
        let path = API.baseURL + "auth/password"
        let header: HTTPHeaders = ["Authorization": "Bearer " + token]
        let params = ["oldPassword":oldPassword, "newPassword":newPassword	]
        
        AF.request(path, method: .put, parameters: params, encoding: JSONEncoding.default, headers: header).response {
            response in
            debugPrint(response)
            let result: Result<Password> = handleResponse(response: response)
            callback(result)
        }
    }
}
