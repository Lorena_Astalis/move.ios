//
//  BookingModel.swift
//  Move
//
//  Created by Lorena Astalis on 13.05.2021.
//

import Foundation

class Booking: Codable {
    let booking: StartBooking
    enum CodingKeys: String, CodingKey {
        case booking = "booking"
    }
}

class StartBooking: Codable {
    let path: [[Double]] = []
    let id: String
    let userId: String
    let scooterId: String
    let startAddress: String
    let startDate: String
    let distance: Double
    let scooterBattery: Int?
    let number: Int
    let v: Int
    
    enum CodingKeys: String, CodingKey {
        case path = "path"
        case id = "_id"
        case userId = "userId"
        case scooterId = "scooterId"
        case startAddress = "startAddress"
        case startDate = "startDate"
        case number = "number"
        case v = "__v"
        case scooterBattery = "scooterBattery"
        case distance = "distance"
    }
}
