//
//  RideModel.swift
//  Move
//
//  Created by Lorena Astalis on 10.05.2021.
//

import Foundation
import CoreLocation

class AllBookings: Codable {
    let bookings: [Ride]
    
    enum CodingKeys: String, CodingKey {
        case bookings = "bookings"
    }
}

class BookingComplete: Codable {
    let booking: Ride
    
    enum CodingKeys: String, CodingKey {
        case booking = "booking"
    }
}

struct Ride: Codable {
    let id: String
    let userId: String
    let scooterId: String
    let startDate: String
    let number: Int
    let v: Int
    let endDate: String?
    let distance: Double?
    let duration: String?
    let totalPrice: Double?
    let startAddress: String?
    let endAddress: String?
    let path: [[Double]]
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case userId = "userId"
        case scooterId = "scooterId"
        case startDate = "startDate"
        case number = "number"
        case v = "__v"
        case endDate = "endDate"
        case distance = "distance"
        case duration = "duration"
        case totalPrice = "totalPrice"
        case path = "path"
        case startAddress = "startAddress"
        case endAddress = "endAddress"
    }
    
    func coordinate() -> CLLocationCoordinate2D {
        CLLocationCoordinate2D(latitude: CLLocationDegrees(path[0][1]), longitude: CLLocationDegrees(path[0][0]))
    }
}
