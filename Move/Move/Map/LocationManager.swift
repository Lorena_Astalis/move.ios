//
//  LocationManager.swift
//  Move
//
//  Created by Lorena Astalis on 19.04.2021.
//

import Foundation
import CoreLocation
import MapKit

class LocationManager: NSObject, ObservableObject {
    @Published var locationManager = CLLocationManager()
    @Published var location: CLLocation?
    @Published var city: String
    private let geocoder = CLGeocoder()
    
    override init() {
        self.city = "Allow location"
        super.init()
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.distanceFilter = kCLDistanceFilterNone
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.startUpdatingLocation()
        self.locationManager.delegate = self
        if let location = locationManager.location {
            self.getCityName(location: location)
        }
    }
    
    func getCityName(location: CLLocation) {
        geocoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) in
                guard let placemark = placemarks?.last else { return }
                if let cityName = placemark.locality {
                    self.city = cityName
                }
            })
    }
}

extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        DispatchQueue.main.async {
            self.location = location
        }
    }
}

extension MKCoordinateRegion {
    static var defaultRegion: MKCoordinateRegion {
        MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 46.770439, longitude: 23.591423), latitudinalMeters: CLLocationDistance.init(400), longitudinalMeters: CLLocationDistance.init(400))
    }
}
