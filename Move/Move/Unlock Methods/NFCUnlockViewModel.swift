//
//  NFCUnlockViewModel.swift
//  Move
//
//  Created by Lorena Astalis on 20.05.2021.
//

import Foundation

class NfcUnlockViewModel: ObservableObject {
    var nfcReader = NFCReader()
    
    init() {
        nfcReader.scan()
    }
    
}
