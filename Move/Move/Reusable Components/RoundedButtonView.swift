//
//  MapButtonView.swift
//  Move
//
//  Created by Lorena Astalis on 19.04.2021.
//

import SwiftUI

struct RoundedButtonView: View {
    let image: Image
    let border: Bool
    let action: () -> Void
    
    var body: some View {
        Button {
            action()
        } label: {
            ZStack {
                if !border {
                    Rectangle()
                        .fill(Color.white)
                        .frame(width: 36, height: 36)
                        .cornerRadius(15)
                        .shadow(color: Color.cloudy.opacity(0.20), radius: 13, x: 7, y: 7)
                } else {
                    RoundedRectangle(cornerRadius: 13)
                        .stroke(Color.cloudy, lineWidth: 0.5)
                        .frame(width: 36, height: 36)
                }
                image
            }
        }
    }
}

struct MapButtonView_Previews: PreviewProvider {
    static var previews: some View {
        RoundedButtonView(image: Image("menu-lines-glyph"), border: true, action: {})
    }
}
