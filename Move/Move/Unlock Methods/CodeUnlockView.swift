//
//  123UnlockView.swift
//  Move
//
//  Created by Lorena Astalis on 27.04.2021.
//

import SwiftUI
import Introspect

enum UnlockType {
    case code
    case qr
    case nfc
}

struct CodeUnlockView: View {
    @ObservedObject var viewModel: CodeUnlockViewModel
    let onClose: () -> Void
    let onStartRide: (Booking) -> Void
    let onOtherUnlockType: (UnlockType) -> Void
    
    init(scooter: Scooter, location: [Double], onClose: @escaping () -> Void, onStartRide: @escaping (Booking) -> Void, onOtherUnlockType: @escaping (UnlockType) -> Void) {
        self.viewModel = CodeUnlockViewModel(scooter: scooter, location: location)
        self.onClose = onClose
        self.onStartRide = onStartRide
        self.onOtherUnlockType = onOtherUnlockType
    }
    
    var body: some View {
        ZStack {
            BackgroundView()
                .edgesIgnoringSafeArea(.all)
            VStack(alignment: .center) {
                StickyHeaderView(backImage: Image("close-glyph"), title: "Enter serial number", image: nil, color: .white, onBack: {
                    onClose()
                })
                Text("Enter the scoter’s\nserial number")
                    .font(.baiJamjureeBold32)
                    .foregroundColor(.white)
                    .multilineTextAlignment(.center)
                    .padding(.vertical, 16)
                    .fixedSize(horizontal: false, vertical: true)
                Text("You can find it on the\n scooter’s front panel")
                    .font(.baiJamjureeMedium16)
                    .foregroundColor(.white)
                    .opacity(0.6)
                    .lineSpacing(3)
                    .fixedSize(horizontal: false, vertical: true)
                Spacer()
                textFields
                    .padding(.vertical, 20)
                Spacer()
                Text("Alternately you can unlock using")
                    .font(.baiJamjureeMedium16)
                    .foregroundColor(.white)
                HStack(spacing: 20) {
                    StrokeButtonView(image: nil, text: "NFC", color: .white, pressed: {
                        onOtherUnlockType(UnlockType.nfc)
                    })
                    .frame(width: 56)
                    Text("or")
                        .font(.baiJamjureeBold16)
                        .foregroundColor(.white)
                    StrokeButtonView(image: nil, text: "QR", color: .white, pressed: {
                        onOtherUnlockType(UnlockType.qr)
                    })
                    .frame(width: 56)
                }.padding(.bottom, 50)

            }
        }
        .onTapGesture {
            hideKeyboard()
        }
    }
    
    var textFields: some View {
        HStack(spacing: 16) {
            textField(character: $viewModel.firstCharacter, isEmpty: viewModel.firstCharacter.isEmpty, isFirstResponder: (viewModel.firstCharacter.isEmpty))
            textField(character: $viewModel.secondCharacter, isEmpty: viewModel.secondCharacter.isEmpty, isFirstResponder: (!viewModel.firstCharacter.isEmpty && viewModel.secondCharacter.isEmpty))
            textField(character: $viewModel.thirdCharacter, isEmpty: viewModel.thirdCharacter.isEmpty, isFirstResponder: (!viewModel.secondCharacter.isEmpty && !viewModel.firstCharacter.isEmpty && viewModel.thirdCharacter.isEmpty))
            textField(character: $viewModel.fourthCharacter, isEmpty: viewModel.fourthCharacter.isEmpty, isFirstResponder: (!viewModel.thirdCharacter.isEmpty && !viewModel.secondCharacter.isEmpty && !viewModel.firstCharacter.isEmpty))
                .onChange(of: viewModel.fourthCharacter, perform: { value in
                    if !(viewModel.firstCharacter.isEmpty && viewModel.secondCharacter.isEmpty && viewModel.thirdCharacter.isEmpty && viewModel.fourthCharacter.isEmpty) {
                        viewModel.onUnlockScooter({ booking in
                                onStartRide(booking)
                        }, { error in
                            // failed to unlock scooter
                            viewModel.firstCharacter = ""
                            viewModel.secondCharacter = ""
                            viewModel.thirdCharacter = ""
                            viewModel.fourthCharacter = ""
                            print(error.localizedDescription)
                            showError(error: error.localizedDescription)
                        })
                    }
                })
        }
        .keyboardType(.numberPad)
    }
    
    func textField(character: Binding<String>, isEmpty: Bool, isFirstResponder: Bool) -> some View {
        TextField("", text: character)
            .accentColor(.indigo)
            .foregroundColor(.indigo)
            .multilineTextAlignment(.center)
            .frame(width: 52, height: 52)
            .background(isEmpty ? Color.cloudy: Color.white)
            .cornerRadius(18)
            .font(.baiJamjureeMedium20)
            .introspectTextField { textfield in
                if isFirstResponder {
                    textfield.becomeFirstResponder()
                } else {
                    textfield.resignFirstResponder()
                }
            }
    }
}

extension View {
    func hideKeyboard() {
            UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
        }
}

struct CodeUnlockView_Previews: PreviewProvider {
    static var previews: some View {
        CodeUnlockView(scooter: Scooter(id: "wer", battery: 34, location: Location(type: "Point", coordinates: [23.4324, 43.4532]), locked: false, internalId: "ewrwe2334233", uniqueNumber: "4343"), location: [43.543, 54.654], onClose: {}, onStartRide: {_ in }, onOtherUnlockType: { _ in })
    }
}
