//
//  AuthenticationCoordinatorView.swift
//  Move
//
//  Created by Lorena Astalis on 07.05.2021.
//

import SwiftUI
import NavigationStack

struct AuthenticationCoordinatorView: View {
    let navigationViewModel: NavigationStack
    let onNext: () -> Void
    
    var body: some View {
        NavigationStackView(navigationStack: navigationViewModel, rootView: {
            OnboardingView(onGetStarted: {
                handleRegistration()
            })
        })
    }
    
    func handleRegistration() {
        navigationViewModel.push(RegisterView( onFinished: {
            handleDriverLicense()
        }, onLogin: {
            handleLogin()
        }))
    }
    
    func handleLogin() {
        navigationViewModel.push(LoginView(
        onForgotPassword: {
            handleForgotPassword()
        }, noAccount: {
            handleRegistration()
        }, onNext: {
            onNext()
        }))
    }
    
    func handleForgotPassword() {
        
    }
    
    func handleDriverLicense() {
        navigationViewModel.push(DriverLicenseValidationView(onBack: {
            // Where to from driver license back button ?
        }, onNext: { image in
            handleUploadDrivingLicense(image: image)
        }))
    }
    
    func handleUploadDrivingLicense(image: Image) {
        API.uploadDriverLicense(image: image, { response in
            print(response)
            switch response {
            case .success:
                print("Success")
                handleSuccessfulUploadDrivingLicense()
                break
            case .failure(let error):
                print("Failure")
                handleFailedToUploadLicense(error: error)
                break
            }
        })
    }
    
    func handleSuccessfulUploadDrivingLicense() {
        navigationViewModel.push(LicenseUploadView(findScooters: {
            onNext()
        }))
    }
    
    func handleFailedToUploadLicense(error: Error) {
        // TODO: implement error state
        showError(error: "Failed to upload" + error.localizedDescription)
    }
}
