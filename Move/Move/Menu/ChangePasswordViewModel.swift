//
//  ChangePasswordViewModel.swift
//  Move
//
//  Created by Lorena Astalis on 19.05.2021.
//

import Foundation

class ChangePasswordViewModel: ObservableObject {
    @Published var oldPassword = ""
    @Published var newPassword = ""
    @Published var confirmedPassword = ""
    
    func changePassword() {
        API.changePassword(oldPassword: oldPassword, newPassword: newPassword, { result in
            switch result {
            case .success:
                showSuccess(message: "Password changed")
                self.reset()
                break
            case .failure(let error):
                showError(error: error.localizedDescription)
                break
            }
        })
    }
    
    var checkNewPassword: Bool {
        return newPassword == confirmedPassword
    }
    
    var allFieldsCompleted: Bool {
        return checkNewPassword && !oldPassword.isEmpty && !newPassword.isEmpty && !confirmedPassword.isEmpty
    }
    
    func reset() {
        self.oldPassword = ""
        self.newPassword = ""
        self.confirmedPassword = ""
    }
}
