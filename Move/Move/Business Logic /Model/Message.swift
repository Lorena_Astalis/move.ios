//
//  Message.swift
//  Move
//
//  Created by Lorena Astalis on 13.05.2021.
//

import Foundation

class Message: Codable {
    var message: String
    
    enum CodingKeys: String, CodingKey {
        case message = "message"
    }
}
