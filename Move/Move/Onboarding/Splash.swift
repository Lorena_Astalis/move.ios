//
//  Splash.swift
//  Move
//
//  Created by Lorena Astalis on 06.04.2021.
//

import SwiftUI

extension Color {
    static var deepPurple: Color {
        return Color("DeepPurple")
    }
    
    static var violet: Color {
        return Color("Violet")
    }

}

struct Splash: View {
    var body: some View {
        ZStack {
            Color.deepPurple
                .ignoresSafeArea()
            Rectangle()
                .fill(Color.white)
                .frame(width: 238, height: 238)
                .opacity(0.1)
                .cornerRadius(93.5)
                .rotationEffect(Angle(degrees: 315))
            Image("pin")
                .frame(width: 120, height: 125)
            GeometryReader { geometry in
                Image("eScooter")
                    .frame(width: 645, height: 684)
                    .offset(x: -317, y: 64)
            }
            logo
        }
    }
    
    var logo: some View {
        HStack(spacing: 0) {
            Image("logo-m")
                .frame(width: 56.09, height: 38.52)
                .padding(5.78)
            Image("logo-o")
                .frame(width: 38.3, height: 39.46)
            Image("logo-v")
                .frame(width: 35.06, height: 38.26)
            Image("logo-e")
                .frame(width: 35.71, height: 39.46)
        }
        .frame(width: 175, height: 40)
    }
}

struct Splash_Previews: PreviewProvider {
    static var previews: some View {
        Splash()
    }
}
