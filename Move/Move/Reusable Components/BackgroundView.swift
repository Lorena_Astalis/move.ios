//
//  Background.swift
//  Move
//
//  Created by Lorena Astalis on 13.04.2021.
//

import SwiftUI

struct BackgroundView: View {
    var body: some View {
        GeometryReader { geometry in
            Color.deepPurple
                .ignoresSafeArea()
            Rectangle()
                .fill(Color.white)
                .opacity(0.05)
                .frame(width: 327, height: 327)
                .cornerRadius(94)
                .rotationEffect(Angle(degrees: -30))
                .position(x: geometry.size.height / 2, y: geometry.size.width / 3)
            Rectangle()
                .fill(Color.white)
                .opacity(0.05)
                .frame(width: 423, height: 423)
                .cornerRadius(164)
                .rotationEffect(Angle(degrees: 4))
                .position(x: 0, y: geometry.size.width + geometry.size.width - 150)
        }
    }
}

struct BackgroundView_Previews: PreviewProvider {
    static var previews: some View {
        BackgroundView()
    }
}
