//
//  ChangePasswordView.swift
//  Move
//
//  Created by Lorena Astalis on 19.05.2021.
//

import SwiftUI

struct ChangePasswordView: View {
    @StateObject var viewModel = ChangePasswordViewModel()
    @State var waiting = false
    @State var isFocused = [false, false, false]
    let onBack: () -> Void
    
    var body: some View {
        VStack {
            StickyHeaderView(backImage: Image("arrow-left-glyph"), title: "Account", image: nil, color: .indigo, onBack: {
                onBack()
            })
            .padding(.bottom, 24)
            VStack {
                CustomTextFieldView(id: 0, text: $viewModel.oldPassword, image: nil, placeholder: "Old password", isSecured: true, isFocused: $isFocused, secure: true, underComment: nil, isVisible: true, error: false, errorMessage: nil, color: Color.cloudy)
                    .padding(.bottom, 12)
                CustomTextFieldView(id: 1, text: $viewModel.newPassword, image: nil, placeholder: "New password", isSecured: true, isFocused: $isFocused, secure: true, underComment: nil, isVisible: true, error: false, errorMessage: nil, color: Color.cloudy)
                    .padding(.bottom, 12)
                CustomTextFieldView(id: 2, text: $viewModel.confirmedPassword, image: nil, placeholder: "Confirm new password", isSecured: true, isFocused: $isFocused, secure: true, underComment: nil, isVisible: true, error: !viewModel.checkNewPassword, errorMessage: "Confirmed password does not match", color: Color.cloudy)
                    .padding(.bottom, 12)
            }
            .padding(.horizontal, 24)
            Spacer()
            ButtonView(text: "Save edits", activateButton: viewModel.allFieldsCompleted, callback: {
                viewModel.changePassword()
            }, image: nil, color: .cloudy, waiting: waiting)
            .padding(24)
        }
    }
}

struct ChangePasswordView_Previews: PreviewProvider {
    static var previews: some View {
        ChangePasswordView(onBack: {})
    }
}
