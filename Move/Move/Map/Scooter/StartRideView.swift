//
//  StartRideView.swift
//  Move
//
//  Created by Lorena Astalis on 29.04.2021.
//

import SwiftUI

struct StartRideView: View {
    @State var waiting = false
    let scooter: Scooter
    
    var body: some View {
        ZStack {
            Color.deepPurple.ignoresSafeArea()
            GeometryReader { geometry in
                ZStack(alignment: .top) {
                    RoundedRectangle(cornerRadius: 29)
                        .fill(Color.white)
                    Image("pink-dash-glyph")
                    VStack {
                        HStack {
                            detailes
                            Spacer()
                            Image("unlock-scooter-glyph")
                        }
                        ButtonView(text: "Start ride", activateButton: true, callback: {
                            // API call on failure waiitng = false -> display error
                            waiting = true
                        }, image: nil, color: Color.raspberry, waiting: waiting)
                        .padding(24)
                    }
                }
                .frame(width: geometry.size.width)
                .offset(y: geometry.size.height - 350)
            }
        }
    }
    
    var detailes: some View {
        VStack(alignment: .leading, spacing: 4) {
            Text("Scooter")
                .font(.baiJamjureeMedium16)
                .foregroundColor(Color.indigo.opacity(0.6))
            Text("#\(scooter.uniqueNumber)")
                .font(.baiJamjureeBold32)
                .foregroundColor(.indigo)
            HStack(spacing: 5) {
                BatteryView(batteryLevel: scooter.battery, expanded: false)
                }
            }
        .padding(24)
        }
}


struct StartRideView_Previews: PreviewProvider {
    static var previews: some View {
        StartRideView(scooter: Scooter(id: "w3e523425t54535", battery: 45, location: Location(type: "Point", coordinates: [32.2342, 35.342]), locked: false, internalId: "43625825233", uniqueNumber: "14A6"))
    }
}
