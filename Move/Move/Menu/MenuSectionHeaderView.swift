//
//  MenuSectionHeaderView.swift
//  Move
//
//  Created by Lorena Astalis on 22.04.2021.
//

import SwiftUI

struct MenuSectionHeaderView: View {
    let image: Image
    let title: String
    
    var body: some View {
        HStack {
            image
            Text(title)
                .font(.baiJamjureeBold18)
                .foregroundColor(.indigo)
                
        }.alignmentGuide(HorizontalAlignment.leading, computeValue: { dimension in
            dimension[HorizontalAlignment.leading]
        })
    }
}

struct MenuSectionHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        MenuSectionHeaderView(image: Image("nut-glyph"), title: "General settings")
    }
}
