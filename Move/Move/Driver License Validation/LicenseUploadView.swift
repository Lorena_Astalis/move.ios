//
//  LicenseChecked.swift
//  Move
//
//  Created by Lorena Astalis on 15.04.2021.
//

import SwiftUI

struct LicenseUploadView: View {
    let findScooters: () -> Void
    var body: some View {
        ZStack(alignment: .center) {
            BackgroundView()
                .ignoresSafeArea(.all)
            VStack {
                Spacer()
                Image("checked-glyph")
                    .padding(28)
                Text("We’ve succesfuly uploaded your driving license")
                    .font(.baiJamjureeBold32)
                    .foregroundColor(.white)
                    .multilineTextAlignment(.center)
                    .padding(28)
                Spacer()
                ButtonView(text: "Find scooters", activateButton: true, callback: {
                    findScooters()
                }, image: nil, color: Color.white, waiting: false)
                .padding(24)
            }
        }
    }
}

struct LicenseChecked_Previews: PreviewProvider {
    static var previews: some View {
        LicenseUploadView(findScooters: {})
    }
}
