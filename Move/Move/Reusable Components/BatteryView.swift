//
//  BatteryView.swift
//  Move
//
//  Created by Lorena Astalis on 04.05.2021.
//

import SwiftUI

struct BatteryView: View {
    let batteryLevel: Int
    let expanded: Bool
    
    var body: some View {
        VStack {
            HStack(spacing: 5) {
                switch batteryLevel {
                case 0..<20:
                    Image("battery-empty-glyph")
                case 20..<50:
                    Image("battery-20-glyph")
                case 50..<70:
                    Image("battery-60-glyph")
                case 70..<90:
                    Image("battery-80-glyph")
                default:
                    Image("battery-full-glyph")
                }
                if !expanded {
                    Text("\(batteryLevel)%")
                        .font(.baiJamjureeMedium14)
                        .foregroundColor(.indigo)
                } else {
                    Text("Battery")
                        .font(.baiJamjureeMedium16)
                        .foregroundColor(.cloudy)
                }
            }
            if expanded {
                Text("\(batteryLevel)%")
                    .font(.baiJamjureeBold44)
                    .foregroundColor(.indigo)
            }
        }
    }
}

struct BatteryView_Previews: PreviewProvider {
    static var previews: some View {
        BatteryView(batteryLevel: 45, expanded: false)
    }
}
