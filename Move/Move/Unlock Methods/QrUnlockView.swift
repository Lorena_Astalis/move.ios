//
//  QrUnlockView.swift
//  Move
//
//  Created by Lorena Astalis on 19.05.2021.
//

import SwiftUI
import CodeScanner


struct QrUnlockView: View {
    @ObservedObject var viewModel: QrUnlockViewModel
    let onClose: () -> Void
    let onOtherUnlockType: (UnlockType) -> Void
    
    init(scooter: Scooter, location: [Double], onClose: @escaping () -> Void, onOtherUnlockType: @escaping (UnlockType) -> Void, onNext: @escaping (Booking) -> Void) {
        viewModel = QrUnlockViewModel(scooter: scooter, location: location, onNext: onNext)
        self.onClose = onClose
        self.onOtherUnlockType = onOtherUnlockType
    }
    
    var body: some View {
        ZStack {
            CodeScannerView(codeTypes: [.qr]) { result in
                viewModel.handleScan(result: result)
            }.edgesIgnoringSafeArea(.all)
            Color.black.opacity(0.6)
                .edgesIgnoringSafeArea(.all)
            VStack(alignment: .center) {
                StickyHeaderView(backImage: Image("close-glyph"), title: "Unlock scooter", image: Image("bulb-glyph"), color: .white, onBack: {
                    onClose()
                })
                Text("Scan QR")
                    .font(.baiJamjureeBold32)
                    .foregroundColor(.white)
                    .multilineTextAlignment(.center)
                    .padding(.vertical, 16)
                Text("You can find it on the\nscooter’s front panel")
                    .font(.baiJamjureeMedium16)
                    .foregroundColor(.white)
                    .opacity(0.6)
                    .lineSpacing(3)
                    .multilineTextAlignment(.center)
                    .fixedSize(horizontal: false, vertical: true)
                Spacer()
                RoundedRectangle(cornerRadius: 29)
                    .stroke(Color.white, lineWidth: 2)
                    .padding(24)
                    .frame(height: 327)
                Spacer()
                Text("Alternately you can unlock using")
                    .font(.baiJamjureeMedium16)
                    .foregroundColor(.white)
                HStack(spacing: 20) {
                    StrokeButtonView(image: nil, text: "123", color: .white, pressed: {
                        onOtherUnlockType(UnlockType.code)
                    })
                    .frame(width: 56)
                    Text("or")
                        .font(.baiJamjureeBold16)
                        .foregroundColor(.white)
                    StrokeButtonView(image: nil, text: "NFC", color: .white, pressed: {
                        onOtherUnlockType(UnlockType.nfc)
                    })
                    .frame(width: 56)
                }.padding(.bottom, 50)

            }
        }
    }
}

struct QrUnlockView_Previews: PreviewProvider {
    static var previews: some View {
        QrUnlockView(scooter: Scooter(id: "234", battery: 34, location: Location(type: "Point", coordinates: []), locked: true, internalId: "342342", uniqueNumber: "2344"), location: [], onClose: {}, onOtherUnlockType: { _ in }, onNext: { _ in})
    }
}
