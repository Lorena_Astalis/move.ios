//
//  TextFields.swift
//  Move
//
//  Created by Lorena Astalis on 13.04.2021.
//

import SwiftUI

struct CustomTextFieldView: View {
    let id: Int
    @Binding var text: String
    let image: [Image]?
    let placeholder: String
    let isSecured: Bool
    @Binding var isFocused: [Bool]
    @State var secure = true
    let underComment: String?
    @State var isVisible = false
    let error: Bool
    let errorMessage: String?
    let color: Color
    
    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            if isSecured {
                if secure {
                    if isFocused[id] {
                        UnderlinedSecuredTextField(isFocused: $isFocused, text: $text, id: id, placeholder: placeholder, underComment: underComment, error: error, errorMessage: errorMessage, color: color)
                            .overlay(
                                image?[0]
                                    .padding(.bottom, 10)
                                    .onTapGesture {
                                        secure = false
                                    }, alignment: .trailing)
                            .introspectTextField { textfield in
                                if isFocused[id] {
                                    textfield.becomeFirstResponder()
                                }
                            }
                    } else {
                        UnderlinedSecuredTextField(isFocused: $isFocused, text: $text, id: id, placeholder: placeholder, underComment: underComment, error: error, errorMessage: errorMessage, color: color)
                            .introspectTextField { textfield in
                                if isFocused[id] {
                                    textfield.becomeFirstResponder()
                                }
                            }
                    }
                } else {
                    if isFocused[id] {
                        UnderlinedTextField(isFocused: $isFocused, text: $text, id: id, placeholder: placeholder, underComment: underComment, error: error, color: color)
                        .overlay(
                            image?[1]
                                .padding(.bottom, 10)
                                .onTapGesture {
                                    secure = true
                                }, alignment: .trailing)
                            .introspectTextField { textfield in
                                if isFocused[id] {
                                    textfield.becomeFirstResponder()
                                }
                            }
                    } else {
                        UnderlinedTextField(isFocused: $isFocused, text: $text, id: id, placeholder: placeholder, underComment: underComment, error: error, color: color)
                            .introspectTextField { textfield in
                                if isFocused[id] {
                                    textfield.becomeFirstResponder()
                                }
                            }
                    }
                }
            } else {
                if isFocused[id] {
                    UnderlinedTextField(isFocused: $isFocused, text: $text, id: id, placeholder: placeholder, underComment: underComment, error: error, color: color)
                    .overlay(
                        image?[0]
                            .padding(.bottom, 10)
                            .onTapGesture {
                                text = ""
                                isFocused[id] = false
                            }, alignment: .trailing)
                        .introspectTextField { textfield in
                            if isFocused[id] {
                                textfield.becomeFirstResponder()
                            }
                        }
                } else {
                    UnderlinedTextField(isFocused: $isFocused, text: $text, id: id, placeholder: placeholder, underComment: underComment, error: error, color: color)
                        .introspectTextField { textfield in
                            if isFocused[id] {
                                textfield.becomeFirstResponder()
                            }
                        }
                }
            }
            if error {
                if let message = errorMessage {
                    Text(message)
                        .font(.baiJamjureeRegular12)
                        .foregroundColor(.red)
                        .padding(.top, -10)
                }
            } else {
                if let comment = underComment {
                    if isFocused[id] && text.isEmpty {
                        Text(isFocused[id] ? comment : "")
                            .font(.baiJamjureeRegular12)
                            .foregroundColor(.white)
                            .padding(.top, -15)
                    }
                }
            }
        }
        .keyboardType(id == isFocused.count - 1 ? .emailAddress : .URL)
        
    }
}

extension View {
    func underlinedTextField(_ error: Bool, color: Color) -> some View {
        if !error {
            return self
                .padding(.vertical, 10)
                .overlay(Rectangle().frame(height: 2).padding(.top, 30).opacity(0.6))
                .foregroundColor(color)
                .accentColor(color)
                .font(.baiJamjureeMedium16)
                .disableAutocorrection(true)
        } else {
            return self
            .padding(.vertical, 10)
            .overlay(Rectangle().frame(height: 2).padding(.top, 30).opacity(0.6))
            .foregroundColor(.trueRed)
            .accentColor(color)
            .font(.baiJamjureeMedium16)
            .disableAutocorrection(true)
        }
    }
    
    func underlinedFocusedTextField(_ error: Bool, color: Color) -> some View {
        if !error {
            return self
            .padding(.vertical, 10)
            .overlay(Rectangle().frame(height: 2).padding(.top, 30))
            .foregroundColor(color)
            .accentColor(color)
            .font(.baiJamjureeMedium16)
            .disableAutocorrection(true)
        } else {
            return self
            .padding(.vertical, 10)
            .overlay(Rectangle().frame(height: 2).padding(.top, 30))
            .foregroundColor(.trueRed)
            .accentColor(color)
            .font(.baiJamjureeMedium16)
            .disableAutocorrection(true)
        }
    }
}

struct UnderlinedTextField: View {
    @Binding var isFocused: [Bool]
    @Binding var text: String
    let id: Int
    let placeholder: String
    let underComment: String?
    let error: Bool
    let color: Color
    
    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            ZStack(alignment: .leading) {
                // Placeholder
                if text.isEmpty && !isFocused[id] {
                    Text(placeholder)
                        .foregroundColor(color == Color.cloudy ? Color.indigo : color)
                        .opacity(0.6)
                        .font(.baiJamjureeMedium16)
                        .padding(.bottom, 10)
                } else {
                    Text(placeholder)
                        .foregroundColor(color == Color.cloudy ? Color.indigo : color)
                        .opacity(0.6)
                        .font(.baiJamjureeRegular12)
                        .padding(.bottom, 35)
                }
                // Focused or not focused logic
                if isFocused[id] {
                    TextField("", text: $text)
                        .foregroundColor(color == Color.cloudy ? Color.indigo : color)
                        .underlinedFocusedTextField(error, color: color)
                } else {
                    TextField("", text: $text)
                        .foregroundColor(color == Color.cloudy ? Color.indigo : color)
                        .onTapGesture {
                            isFocused = isFocused.map{ _ in false }
                            isFocused[id] = true
                        }
                        .underlinedTextField(error, color: color)
        
                }
            }
           
        }
        .padding(.vertical, 10)
    }
}


struct UnderlinedSecuredTextField: View {
    @Binding var isFocused: [Bool]
    @Binding var text: String
    let id: Int
    let isVisible = false
    let placeholder: String
    let underComment: String?
    let error: Bool
    let errorMessage: String?
    let color: Color
    
    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            ZStack(alignment: .leading) {
                // Placeholder
                if text.isEmpty && !isFocused[id] {
                    Text(placeholder)
                        .foregroundColor(color)
                        .opacity(0.6)
                        .font(.baiJamjureeMedium16)
                        .padding(.bottom, 10)
                } else {
                    Text(placeholder)
                        .foregroundColor(color)
                        .opacity(0.6)
                        .font(.baiJamjureeRegular12)
                        .padding(.bottom, 35)
                }
                if isVisible {
                    // Focused or not focused logic
                    if isFocused[id] {
                        TextField("", text: $text)
                            .foregroundColor(color == Color.cloudy ? Color.indigo : color)
                            .underlinedFocusedTextField(error, color: color)
                    } else {
                        TextField("", text: $text)
                            .foregroundColor(color == Color.cloudy ? Color.indigo : color)
                            .onTapGesture {
                                isFocused = isFocused.map{ _ in false }
                                isFocused[id] = true
                            }
                            .underlinedTextField(error, color: color)
                    }
                } else {
                    // Focused or not focused logic
                    if isFocused[id] {
                        SecureField("", text: $text)
                            .foregroundColor(color == Color.cloudy ? Color.indigo : color)
                            .underlinedFocusedTextField(error, color: color)
                    } else {
                        SecureField("", text: $text)
                            .foregroundColor(color == Color.cloudy ? Color.indigo : color)
                            .onTapGesture {
                                isFocused = isFocused.map{ _ in false }
                                isFocused[id] = true
                            }
                            .underlinedTextField(error, color: color)
                    }
                }
            }
            
        }
        .padding(.vertical, 10)
    }
}

struct CustomTextField_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            CustomTextFieldView(id: 0, text: .constant(""), image: [Image("open-eye-glyph"), Image("closed-eye-glyph")], placeholder: "Password", isSecured: false, isFocused: .constant([false]), underComment: nil, error: false, errorMessage: "error", color: Color.pink)
                .background(Color.black)
        }
    }
}
