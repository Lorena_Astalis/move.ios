//
//  AuthModel.swift
//  Move
//
//  Created by Lorena Astalis on 13.05.2021.
//

import Foundation

struct Auth: Decodable {
    var token: String
    var user: User
}

struct Password: Decodable {
    let success: Bool
    
    enum CodingKeys: String, CodingKey {
        case success = "success"
    }
}
