//
//  UnlockModel.swift
//  Move
//
//  Created by Lorena Astalis on 29.04.2021.
//

import Foundation


//{"scooter":{"location":{"type":"Point","coordinates":[23.593054,46.767434]},"locked":false,"status":"Active","_id":"607941c0202797d521a060de","battery":45,"lastSeenDate":"2021-04-21T09:01:50.236Z","internalId":"918960134503883","uniqueNumber":"3883","dummy":"Dummy","charging":"Not Charging","online":"Online","booked":"Booked"}}

struct UpdatedLockedStatus: Codable {
    let id: String
    let location: Location
    let locked: Bool
    let battery: Int
    let lastSeenDate: String
    let internalId: String
    let uniqueNumber: String
    let dummy: Bool
    let online: Bool
    let status: String
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case location = "location"
        case locked = "locked"
        case battery = "battery"
        case lastSeenDate = "lastSeenDate"
        case internalId = "internalId"
        case uniqueNumber = "uniqueNumber"
        case dummy = "dummy"
        case online = "online"
        case status = "status"
    }
}

struct LockUnlockResponse: Codable {
    let scooterStatus: UpdatedLockedStatus
    
    enum CodingKeys: String, CodingKey {
        case scooterStatus = "scooter"
    }
}
