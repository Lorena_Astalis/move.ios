//
//  NFCReader.swift
//  Move
//
//  Created by Lorena Astalis on 20.05.2021.
//

import Foundation
import CoreNFC
import CoreLocation

class NfcViewModel: NSObject, NFCNDEFReaderSessionDelegate, ObservableObject {
    @Published var location: [Double]
    @Published var startAddress = ""
    var session: NFCNDEFReaderSession?
    let onNext: (Booking) -> Void
    
    func readerSession(_ session: NFCNDEFReaderSession, didInvalidateWithError error: Error) {
        showError(error: error.localizedDescription)
    }
    
    func readerSession(_ session: NFCNDEFReaderSession, didDetectNDEFs messages: [NFCNDEFMessage]) {
        
        for message in messages {
            for record in message.records {
                if let string = String(data: record.payload, encoding: .ascii) {
                    showSuccess(message: string)
                    let afterEqual = string.firstIndex(of: "=")
                    print(string.index(after: afterEqual!))
                    // sbjelna001
                    if (string == "sbjelna001") {
                        API.nfcUnlockScooter(code: "4352", address: startAddress, { result in
                            switch result {
                            case .success(let booking):
                                self.onNext(booking)
                                break
                            case .failure(let error):
                                showError(error: error.localizedDescription)
                            }
                        })
                    }
                }
            }
        }
    }
    
    init (location: [Double], onNext: @escaping (Booking) -> Void) {
        self.location = location
        self.onNext = onNext
        super.init()
        self.session = NFCNDEFReaderSession(delegate: self, queue: DispatchQueue.main, invalidateAfterFirstRead: true)
        session?.begin()
        getLocation()
    }
    
    func getLocation() {
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(CLLocation(latitude: location[0], longitude: location[1]),
            completionHandler: { (placemarks, error) in
                guard let placemark = placemarks?.last else {
                    return
                }
                print(placemark)
                if let street = placemark.thoroughfare {
                    self.startAddress = street
                }
                
                if let number = placemark.subThoroughfare {
                    self.startAddress += ", \(number)"
                }
            })
    }
}

