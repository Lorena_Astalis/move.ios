//
//  ContentView.swift
//  Move
//
//  Created by Lorena Astalis on 07.04.2021.
//

import SwiftUI
import NavigationStack

struct ContentView: View {
    var body: some View {
        CoordinatorView()
    }
}

struct CoordinatorView: View {
    var navigationViewModel: NavigationStack = NavigationStack(easing: Animation.linear)
    
    var body: some View {
        if Session.shared.isValidSession{
            handleMap()
                .onAppear {
                    print(Session.shared.authToken ?? "No token")
                }
        } else {
            handleAuthentification()
                .onAppear {
                    print(Session.shared.authToken ?? "No token")
                }
        }
    }
    
    func handleAuthentification() -> some View {
        NavigationStackView(transitionType: .default, navigationStack: navigationViewModel) {
            AuthenticationCoordinatorView(navigationViewModel: navigationViewModel, onNext: {
                navigationViewModel.push(MapCoordinatorView(navigationViewModel: navigationViewModel, onMenu: handleMenu), withId: "map")
            })
        }
    }
    
    func handleMap() -> some View {
        NavigationStackView(transitionType: .default, navigationStack: navigationViewModel) {
            handleAuthentification()
        }
        .onAppear {
            navigationViewModel.push(MapCoordinatorView(navigationViewModel: navigationViewModel, onMenu: handleMenu), withId: "map")
        }
    }
  
    func handleMenu() {
        navigationViewModel.push(MenuCoordinatorView(navigationViewModel: navigationViewModel))
    }    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
