//
//  ScooterUnlockViewModel.swift
//  Move
//
//  Created by Lorena Astalis on 21.05.2021.
//

import Foundation

class ScooterUnlockViewModel: ObservableObject {
    @Published var disablePing = false
    let scooter: Scooter
    let location: [Double]
    
    init(scooter: Scooter, location: [Double]) {
        self.scooter = scooter
        self.location = location
    }
    
    func pingScooter() {
        // TODO: Add check for 40m distance, if not, disable button
        if(checkDistance() > 0.4) {
            disablePing = true
            return
        }
        API.pingScooter(scooterId: scooter.uniqueNumber, lat: location[0], long: location[1], callback: { result in
            switch result {
            case .success: break
            case .failure(let error):
                showError(error: error.localizedDescription)
                break
            }
            
        })
    }
    
    func checkDistance() -> Double {
        // Haversine formula
        let R: Double = 6371 // Radius of the earth in km
        let dLat = deg2rad(scooter.location.coordinates[1] - location[0])
        let dLon = deg2rad(scooter.location.coordinates[0] - location[1])
        let a = sin(dLat/2) * sin(dLat/2) + cos(deg2rad(location[0])) * cos(deg2rad(scooter.location.coordinates[1])) * sin(dLon/2) * sin(dLon/2)
        let c = 2 * atan2(sqrt(a), sqrt(1 - a))
        let d = R * c
        return d
    }
    
    func deg2rad(_ number: Double) -> Double {
        return number * .pi / 180
    }
}
