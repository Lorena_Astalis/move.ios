//
//  PaymentPreview.swift
//  Move
//
//  Created by Lorena Astalis on 11.05.2021.
//

import SwiftUI
import MapKit
import PassKit

struct TripSummaryView: View {
    @State private var region = MKCoordinateRegion(center: CLLocationCoordinate2D(), span: MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005))
    @State private var location = CLLocationCoordinate2D(latitude: 46.75618, longitude: 23.5948)
    let paymentHandler = PaymentHandler()
    let ride: BookingComplete
    let onNext: () -> Void
    
    var body: some View {
        ZStack {
            Color.white
                .edgesIgnoringSafeArea(.all)
            VStack {
                title
                MapPolylineView(centerCoordinate: $location, locations: getCoordinates())
                    .frame(maxWidth: .infinity, maxHeight: 172)
                    .clipShape(RoundedRectangle(cornerRadius: 29))
                    .padding(.horizontal, 22)
                detailes
                Spacer()
                button
            }
        }
        .onAppear {
            region.center = ride.booking.coordinate()
            // location = CLLocationCoordinate2D(latitude:ride.booking.path[0][1], longitude: ride.booking.path[0][0])
        }
        .statusBarStyle(.darkContent)
    }
    
    var title: some View {
        Text("Trip Summary")
            .font(.baiJamjureeSemiBold17)
            .foregroundColor(.indigo)
            .padding(24)
            .padding(.bottom, 24)
    }
    
    var map: some View {
        Map(coordinateRegion: $region)
            .frame(maxWidth: .infinity, maxHeight: 172)
            .clipShape(RoundedRectangle(cornerRadius: 29))
            .padding(.horizontal, 22)
    }
    
    var detailes: some View {
        VStack {
            VStack(alignment: .leading) {
                displayText(title: "From", content: ride.booking.startAddress ?? "Not available")
                    .padding(.bottom, 12)
                displayText(title: "To", content: ride.booking.endAddress ?? "Not available")
                    .padding(.bottom, 12)
            }
            .padding(.horizontal, 20)
            .padding(.vertical, 12)
            .frame(idealWidth: .infinity, maxWidth: .infinity, alignment: .leading)
            .background(
                RoundedRectangle(cornerRadius: 16)
                    .fill(Color.cloudy.opacity(0.15))
            )
            .padding(.vertical, 36)
            HStack(alignment: .top) {
                detailesSection(image: Image("clock-glyph"), title: "Travel time", content: "\(ride.booking.duration?.prefix(5) ?? "" ) min")
                Spacer()
                detailesSection(image: Image("map-glyph"), title: "Distance", content: String(format: "%.1f km", ride.booking.distance ?? 0.0))
                Spacer()
            }
        }
        .padding(.horizontal, 24)
        
    }
    
    var button: some View {
        ApplePayButtonView(callback: {
            self.paymentHandler.startPayment(price: "\(ride.booking.totalPrice ?? 0.00)") { success in
                if success {
                    onNext()
                    showSuccess(message: "You can find your recipt in your mail")
                   
                } else {
                    showError(error: "We couln't process your payment")
                    onNext()
                }
            }
        })
        .padding(24)
    }
    
    func displayText(title: String, content: String) -> some View {
        VStack(alignment: .leading, spacing: 3) {
            Text(title)
                .font(.baiJamjureeMedium14)
                .foregroundColor(.cloudy)
            Text(content)
                .font(.baiJamjureeBold16)
                .foregroundColor(.indigo)
        }
    }
    
    func detailesSection(image: Image, title: String, content: String) -> some View {
        HStack(alignment: .top) {
            image
            VStack(alignment: .leading) {
                Text(title)
                    .font(.baiJamjureeMedium16)
                    .foregroundColor(.cloudy)
                Text(content)
                    .font(.baiJamjureeBold16)
                    .foregroundColor(.indigo)
            }
        }
    }
    
    func getCoordinates() -> [CLLocationCoordinate2D] {
        var coordinates: [CLLocationCoordinate2D] = []
        for index in 0..<ride.booking.path.count {
            coordinates.append(CLLocationCoordinate2D(latitude: ride.booking.path[index][1], longitude: ride.booking.path[index][0]))
        }
        return coordinates
    }
}

//struct PaymentPreview_Previews: PreviewProvider {
//    static var previews: some View {
////        TripSummaryView(ride: BookingComplete(booking: Ride(id: "234234234", userId: "34534543", scooterId: "234324324", startDate: "23432432", number: 3, v: 4, endDate: "432", distance: 3.4, duration: "00:34:00", totalPrice: 43.4, startAddress: "", endAddress: "", path: [[23.423, 46.234], [23.423, 46.234]])), onNext: {})
//    }
//}
