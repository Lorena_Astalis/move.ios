//
//  Validator.swift
//  Move
//
//  Created by Lorena Astalis on 12.04.2021.
//

import Foundation

enum UserRegisterError: Error {
    case invalidEmail
    case invalidPassword
    
    var description: String {
        switch self {
        case .invalidEmail:
            return "Please enter a valid email"
        case .invalidPassword:
            return "Password must be at least 8 characters long, and must contain at least 1 upperchase character, 1 lowercase character, 1 digit and 1 symbol."
        }
    }
}

struct UserRegisterValidator {
    static let EMAIL_REGEX_PATTERN = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    static let PASSWORD_REGEX_PATTERN = "^.*(?=.{8,})(?=.*[A-Z])(?=.*[a-zA-Z])(?=.*\\d)|(?=.*[!#*$%&? ]).*$"
    
    static func validation(email: String, password: String) throws {
        let emailPred = NSPredicate(format:"SELF MATCHES %@", EMAIL_REGEX_PATTERN)
        if !emailPred.evaluate(with: email) {
                throw UserRegisterError.invalidEmail
        }
        let passwordPred = NSPredicate(format:"SELF MATCHES %@", PASSWORD_REGEX_PATTERN)
        if !passwordPred.evaluate(with: password) {
            print("Password not valid")
            throw UserRegisterError.invalidPassword
        }
    }
}
