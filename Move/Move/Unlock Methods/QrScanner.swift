//
//  QrScanner.swift
//  Move
//
//  Created by Lorena Astalis on 20.05.2021.
//

import Foundation
import CodeScanner
import CoreLocation

class QrUnlockViewModel: ObservableObject {
    var scooter: Scooter
    var location: [Double]
    var startAddress: String = ""
    let onNext: (Booking) -> Void
    
    init(scooter: Scooter, location: [Double], onNext: @escaping (Booking) -> Void) {
        self.scooter = scooter
        self.location = location
        self.onNext = onNext
        getLocation()
    }
    
    func handleScan(result: Swift.Result<String, CodeScanner.CodeScannerView.ScanError>) {
        switch result {
        case .success(let code):
            onUnlockScooter(code: code, { booking in
                self.onNext(booking)
            }, { error in
                showError(error: error.localizedDescription)
            })
            
        case .failure:
            showError(error: "Scanning failed")
        }
    }
    
    func onUnlockScooter(code: String,_ success: @escaping (Booking) -> Void, _ failure: @escaping (APIError) -> Void) {
        print(startAddress)
        API.qrUnlockScooter(code: code, address: startAddress) { result in
            switch result {
            case .success(let booking):
                success(booking)
                break
            case .failure(let error):
                failure(error as! APIError)
                break
            }
        }
    }
    
    func getLocation() {
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(CLLocation(latitude: location[0], longitude: location[1]),
            completionHandler: { (placemarks, error) in
                guard let placemark = placemarks?.last else {
                    return
                }
                print(placemark)
                if let street = placemark.thoroughfare {
                    self.startAddress = street
                }
                
                if let number = placemark.subThoroughfare {
                    self.startAddress += ", \(number)"
                }
            })
    }
}
