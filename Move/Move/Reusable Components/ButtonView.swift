//
//  ButtonView.swift
//  Move
//
//  Created by Lorena Astalis on 20.04.2021.
//

import SwiftUI

struct ButtonView: View {
    let text: String
    let activateButton: Bool
    let callback: () -> Void
    let image: Image?
    let color: Color
    let waiting: Bool
    
    
    var body: some View {
        if activateButton && !waiting {
            activeButton
        } else {
            inactiveButton
        }
    }
    
    var inactiveButton: some View {
        VStack {
        if waiting {
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: .cloudy))
                    .frame(height: 56)
                    .frame(maxWidth: .infinity)
                    .overlay(
                        RoundedRectangle(cornerRadius: 16)
                            .strokeBorder(Color.raspberry.opacity(0.3), lineWidth: 1))
                    .padding(4)
        } else {
            HStack {
                Text(text)
                    .font(.baiJamjureeBold16)
                    .foregroundColor(color)
                    .opacity(0.6)
                    .padding(4)
                image
            }
            .frame(height: 56)
            .frame(maxWidth: .infinity)
            .overlay(
                    RoundedRectangle(cornerRadius: 16)
                        .strokeBorder(Color.raspberry.opacity(0.3), lineWidth: 1))
        }
    }
}
    
    var activeButton: some View {
        Button {
            callback()
        } label: {
            HStack {
                Text(text)
                    .font(.baiJamjureeBold16)
                    .foregroundColor(.white)
                    .padding(4)
                 image
            }
            .frame(height: 56)
            .frame(maxWidth: .infinity)
            .background(RoundedRectangle(cornerRadius: 16)
                            .fill(Color.raspberry))
            .shadow(color: Color.raspberry.opacity(0.15), radius: 20, x: 7, y: 7)
        }
    }
}

struct ButtonView_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.black
            ButtonView(text: "Get started", activateButton: false, callback: {}, image: Image("right-arrow-glyph"), color: .cloudy, waiting: true)
        }
    }
}
