//
//  Onboarding\.swift
//  Move
//
//  Created by Lorena Astalis on 07.04.2021.
//

import SwiftUI

struct OnboardingView: View {
    @State var index = 0
    let onGetStarted: () -> Void
    
    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .top) {
                bodyContent
                    .transition(AnyTransition.opacity.animation(.easeInOut(duration: 1.0)))
            }
            //.frame(height: geometry.size.height, alignment: .bottom)
            .padding(.top, -geometry.safeAreaInsets.top)
        }.statusBarStyle(.lightContent)
    }
    
    var bodyContent: some View {
        VStack {
            if index < onboardingData.count {
                    VStack {
                        GeometryReader { geometry in
                            Image(onboardingData[index].image)
                                .resizable()
                                .scaledToFill()
                                .edgesIgnoringSafeArea(.all)
                                .frame(width: geometry.size.width, alignment: .center)
                        }
                        .clipped()
                        OnboardingTextArea(index: $index, onGetStarted: { onGetStarted() })
                        Spacer()
                        OnboardingBottomArea(index: $index, onGetStarted: onGetStarted)
                    }
            }
        }
    }
}


struct OnboardingTextArea: View {
    @Binding var index: Int
    let onGetStarted: () -> Void
    var body: some View {
        if index < onboardingData.count {
            VStack(alignment: .leading, spacing: 1) {
                HStack(alignment: .center) {
                    Text(onboardingData[index].title)
                        .font(.baiJamjureeBold32)
                        .foregroundColor(.indigo)
                        .frame(alignment: .leading)
                        .padding(.vertical)
                    Spacer()
                    if index != 4 {
                    Text("Skip")
                        .font(.baiJamjureeSemiBold14)
                        .foregroundColor(.cloudy)
                        .padding(.vertical)
                        .onTapGesture {
                            onGetStarted()
                        }
                    }
                }
                Text(onboardingData[index].text)
                    .font(.baiJamjureeMedium16)
                    .foregroundColor(.indigo)
                    .opacity(0.7)
                    .frame(alignment: .leading)
            }
            .padding(.horizontal, 24)
        }
    }
}


struct OnboardingBottomArea: View {
    @Binding var index: Int
    let onGetStarted: () -> Void
    var body: some View {
        HStack {
            progress
            Spacer()
            nextButton
        }
        .padding(24)
    }
    
    var nextButton: some View {
        Button {
            index += 1
            if index == 5 {
                onGetStarted()
            }
        } label: {
            HStack {
                Text(index == 4 ? "Get started": "Next")
                    .font(.baiJamjureeBold16)
                    .foregroundColor(.white)
                    .padding(8)
                Image("arrow-right")
            }
            .padding(8)
            .background(Color.raspberry)
            .cornerRadius(16.0)
            .shadow(color: .raspberry.opacity(0.29), radius: 20, x: 7.0, y: 7.0)
        }
    }
    
    var progress: some View {
        HStack(spacing: 12) {
            ForEach(0 ..< 5) { i in
                if i == index {
                    dash
                } else {
                    dot
                }
            }
        }
        .frame(width: 80, height: 4)
        
    }
    
    var dot: some View {
        Rectangle()
            .fill(Color.cloudy)
            .frame(width: 4, height: 4)
            .cornerRadius(1.5)
    }
    
    var dash: some View {
        Rectangle()
            .fill(Color.indigo)
            .frame(width: 16, height: 4)
            .cornerRadius(1.5)
    }
    
}

struct Onboarding_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingView(onGetStarted: {})
    }
}
