//
//  LoginViewModel.swift
//  Move
//
//  Created by Lorena Astalis on 15.04.2021.
//

import Foundation

public class LoginViewModel: ObservableObject {
    @Published var email: String
    @Published var password: String
    
    init() {
        self.email = ""
        self.password = ""
    }
    
    func login(success callbackSuccess: @escaping () -> Void, failure callbackFailure: @escaping () -> Void) {
        API.login(email: self.email, password: self.password){ (result) in
            switch result {
            case .success(let authResult):
                print(authResult.token)
                Session.shared.authToken = authResult.token
                callbackSuccess()
            case .failure(let error):
                print(error.localizedDescription)
                callbackFailure()
            }
        }
    }
}
