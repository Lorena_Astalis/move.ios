//
//  NfcUnlockView.swift
//  Move
//
//  Created by Lorena Astalis on 17.05.2021.
//

import SwiftUI

struct NfcUnlockView: View {
    @ObservedObject var viewModel: NfcViewModel
    let onClose: () -> Void
    let onOtherUnlockType: (UnlockType) -> Void
    
    init(location: [Double], onClose: @escaping () -> Void, onOtherUnlockType: @escaping (UnlockType) -> Void, onNext: @escaping (Booking) -> Void) {
        self.viewModel = NfcViewModel(location: location, onNext: onNext)
        self.onClose = onClose
        self.onOtherUnlockType = onOtherUnlockType
    }
    
    var body: some View {
        ZStack {
            BackgroundView()
                .edgesIgnoringSafeArea(.all)
            VStack(alignment: .center) {
                StickyHeaderView(backImage: Image("close-glyph"), title: "Bring your phone closer", image: nil, color: .white, onBack: {
                    onClose()
                })
                Text("NFC Unlock")
                    .font(.baiJamjureeBold32)
                    .foregroundColor(.white)
                    .multilineTextAlignment(.center)
                    .padding(.vertical, 16)
                Text("Hold your phone close to the NFC Tag\nlocated on top of the handlebar of\nyour scooter.")
                    .font(.baiJamjureeMedium16)
                    .foregroundColor(.white)
                    .opacity(0.6)
                    .lineSpacing(3)
                    .multilineTextAlignment(.center)
                Spacer()
                circle
                Spacer()
                Text("Alternately you can unlock using")
                    .font(.baiJamjureeMedium16)
                    .foregroundColor(.white)
                HStack(spacing: 20) {
                    StrokeButtonView(image: nil, text: "123", color: .white, pressed: {
                        onOtherUnlockType(UnlockType.code)
                    })
                    .frame(width: 56)
                    Text("or")
                        .font(.baiJamjureeBold16)
                        .foregroundColor(.white)
                    StrokeButtonView(image: nil, text: "QR", color: .white, pressed: {
                        onOtherUnlockType(UnlockType.qr)
                    })
                    .frame(width: 56)
                }.padding(.bottom, 50)

            }
        }
    
    }
    
    @State var wave = false
    var circle: some View {
        ZStack {
            Image("move-logo")
            Circle()
                .stroke(Color.white, lineWidth: 3)
                .frame(width: 172, height: 172)
                
            Circle()
                .stroke(Color.white, lineWidth: 2)
                .frame(width: 172, height: 172)
                .scaleEffect(wave ? 2 : 1)
                .opacity(wave ? 0.5 : 1)
                .animation(Animation.easeInOut(duration: 1).repeatForever(autoreverses: true).speed(0.5), value: wave)
                .onAppear {
                    self.wave.toggle()
                }
            Circle()
                .stroke(Color.white, lineWidth: 1)
                .frame(width: 172, height: 172)
                .scaleEffect(wave ? 3 : 1)
                .opacity(wave ? 0.2 : 0.5)
                .animation(Animation.easeInOut(duration: 1).repeatForever(autoreverses: true).speed(0.5), value: wave)
        }
        .ignoresSafeArea()
    }
}

struct NfcUnlockView_Previews: PreviewProvider {
    static var previews: some View {
        NfcUnlockView(
        location: []
        ,onClose: {
            
        }, onOtherUnlockType: { _ in
            
        }, onNext: { _ in
            
        })
    }
}
