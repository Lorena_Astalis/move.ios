//
//  UnlockSuccessfulView.swift
//  Move
//
//  Created by Lorena Astalis on 29.04.2021.
//

import SwiftUI

struct UnlockSuccessfulView: View {
    
    var body: some View {
        ZStack {
            BackgroundView()
                .edgesIgnoringSafeArea(.all)
            VStack {
                Text("Unlock\nsuccessful")
                    .font(.baiJamjureeBold32)
                    .foregroundColor(.white)
                    .multilineTextAlignment(.center)
                    .padding(.vertical, 100)
                    .fixedSize(horizontal: false, vertical: true)
                Image("checked-glyph")
                Text("Please respect all the driving regulations\nand other participants in traffic while using\nour scooters.")
                    .font(.baiJamjureeMedium16)
                    .foregroundColor(Color.white.opacity(0.6))
                    .multilineTextAlignment(.center)
                    .padding(.vertical, 100)
                    .fixedSize(horizontal: false, vertical: true)
            }
        }
    }
}

struct UnlockSuccessfulView_Previews: PreviewProvider {
    static var previews: some View {
        UnlockSuccessfulView()
    }
}
