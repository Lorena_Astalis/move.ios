//
//  CodeUnlockViewModel.swift
//  Move
//
//  Created by Lorena Astalis on 27.04.2021.
//

import Foundation
import CoreLocation


class CodeUnlockViewModel: ObservableObject {
    @Published var firstCharacter: String = "" {
        didSet {
            if firstCharacter.count > 1 {
                firstCharacter = String(firstCharacter.prefix(1))
            }
        }
    }
    
    @Published var secondCharacter = "" {
        didSet {
            if secondCharacter.count > 1 {
                secondCharacter = String(secondCharacter.prefix(1))
            }
        }
    }
    
    @Published var thirdCharacter = "" {
        didSet {
            if thirdCharacter.count > 1 {
                thirdCharacter = String(thirdCharacter.prefix(1))
            }
        }
    }
    
    @Published var fourthCharacter = "" {
        didSet {
            if fourthCharacter.count > 1 {
                fourthCharacter = String(fourthCharacter.prefix(1))
            }
        }
    }
    
    var scooter: Scooter
    var location: [Double]
    var startAddress: String = ""
    
    init(scooter: Scooter, location: [Double]) {
        self.scooter = scooter
        self.location = location
        getLocation()
    }
    
    func onUnlockScooter(_ success: @escaping (Booking) -> Void, _ failure: @escaping (APIError) -> Void) {
        print(startAddress)
        API.codeUnlockScooter(location: location, code: firstCharacter + secondCharacter + thirdCharacter + fourthCharacter, address: startAddress) { result in
            switch result {
            case .success(let booking):
                success(booking)
                break
            case .failure(let error):
                failure(error as! APIError)
                break
            }
        }
    }
    
    func getLocation() {
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(CLLocation(latitude: location[0], longitude: location[1]),
            completionHandler: { (placemarks, error) in
                guard let placemark = placemarks?.last else {
                    return
                }
                print(placemark)
                if let street = placemark.thoroughfare {
                    self.startAddress = street
                }
                
                if let number = placemark.subThoroughfare {
                    self.startAddress += ", \(number)"
                }
            })
    }
}
