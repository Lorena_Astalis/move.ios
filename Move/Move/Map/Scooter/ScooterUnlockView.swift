//
//  ScooterUnlockView.swift
//  Move
//
//  Created by Lorena Astalis on 26.04.2021.
//

import SwiftUI

// TODO: Add ViewModel !!!
struct ScooterUnlockView: View {
    @ObservedObject var viewModel: ScooterUnlockViewModel
    @State var offset = CGFloat(300.0)
    let dragDown: () -> Void
    let onUnlock: (UnlockType) -> Void
    
    init(scooter: Scooter, currentLocation: [Double], dragDown: @escaping () -> Void, onUnlock: @escaping (UnlockType) -> Void) {
        viewModel = ScooterUnlockViewModel(scooter: scooter, location: currentLocation)
        self.dragDown = dragDown
        self.onUnlock = onUnlock
    }
    
    var body: some View {
        GeometryReader { geometry in
            detailes
                .transition(.slide)
                .gesture(DragGesture()
                            .onChanged { value in
                                offset = value.location.y
                            }
                            .onEnded { value in
                                offset = geometry.size.height + 100
                                dragDown()
                            }
                )
                .onAppear {
                    offset = geometry.size.height - 438
                }
        }
    }
    
    var detailes: some View {
        GeometryReader { geometry in
            VStack {
                Spacer()
                ZStack(alignment: .top) {
                    RoundedRectangle(cornerRadius: 29)
                        .fill(Color.white)
                        .frame(width: geometry.size.width)
                    Image("pink-dash-glyph")
                    content
                }
                .offset(y: offset)
                .onAppear {
                    offset = geometry.size.height
                }
            }
        }
    }
    
    var content: some View {
        VStack(alignment: .center) {
            Text("You can unlock this scooter\n through these methods:")
                .font(.baiJamjureeBold16)
                .foregroundColor(.indigo)
                .lineSpacing(5)
                .multilineTextAlignment(.center)
                .padding(.horizontal, 80)
                .padding(.top, 24)
            HStack {
                scooterData
                Spacer()
                Image("unlock-scooter-glyph")
            }
            buttons
        }
    }
    
    var scooterData: some View {
        VStack(alignment: .leading, spacing: 16) {
            Text("Scooter")
                .font(.baiJamjureeMedium16)
                .foregroundColor(.cloudy)
            Text("#\(viewModel.scooter.uniqueNumber)")
                .font(.baiJamjureeBold32)
                .foregroundColor(.indigo)
            BatteryView(batteryLevel: viewModel.scooter.battery, expanded: false)
            HStack {
                RoundedButtonView(image: Image("bell-glyph"), border: true, action: {
                    viewModel.pingScooter()
                })
                .disabled(viewModel.disablePing)
                Text("Ring")
                    .font(.baiJamjureeMedium16)
                    .foregroundColor(.indigo)
            }
            HStack(spacing: 10) {
                RoundedButtonView(image: Image("missing-scooter-glyph"), border: true, action: {
                    // missing
                })
                Text("Missing")
                    .font(.baiJamjureeMedium16)
                    .foregroundColor(.indigo)
            }
            
        }.padding(.leading, 24)
    }
    
    var buttons: some View {
        HStack(spacing: 20) {
            StrokeButtonView(image: nil, text: "NFC", color: Color.raspberry, pressed: {
                onUnlock(UnlockType.nfc)
            })
            StrokeButtonView(image: nil, text: "QR", color: Color.raspberry, pressed: {
                onUnlock(UnlockType.qr)
            })
            StrokeButtonView(image: nil, text: "123", color: Color.raspberry, pressed: {
                onUnlock(UnlockType.code)
            })
            
        }
        .padding(24)
    }

}

struct ScooterUnlockView_Previews: PreviewProvider {
    static var previews: some View {
        ScooterUnlockView(scooter: Scooter(id: "wer354326265546", battery: 23, location: Location(type: "Point", coordinates: [23, 46]), locked: false, internalId: "437659725", uniqueNumber: "2342"), currentLocation: [46.770439, 23.591423], dragDown: {}, onUnlock: {_ in })
    }
}
