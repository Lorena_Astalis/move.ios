//
//  Coordinator.swift
//  Move
//
//  Created by Lorena Astalis on 21.05.2021.
//

import SwiftUI
import VisionKit

class Coordinator: NSObject, VNDocumentCameraViewControllerDelegate {
    @Binding var isPresented: Bool
    @Binding var image: Image?
    var parent: ScanDocumentView
    
    init(parent: ScanDocumentView, image: Binding<Image?>, isPresented: Binding<Bool>) {
        self._isPresented = isPresented
        self._image = image
        self.parent = parent
    }
    
    func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFinishWith scan: VNDocumentCameraScan) {
        
        if scan.pageCount > 0 {
            let extractedImage = scan.imageOfPage(at: 0)
            print("here")
            image = Image(uiImage: extractedImage)
        }
        
    }
}
