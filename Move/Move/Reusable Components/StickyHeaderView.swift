//
//  StickyHeader.swift
//  Move
//
//  Created by Lorena Astalis on 15.04.2021.
//

import SwiftUI

struct StickyHeaderView: View {
    let backImage: Image
    let title: String
    let image: Image?
    let color: Color
    let onBack: () -> Void
    
    var body: some View {
        HStack {
            Button {
                onBack()
            } label: {
                backImage
                    .padding()
            }
//            .frame(width: 24, height: 24)
            HStack {
            Spacer()
            Text(title)
                .font(.baiJamjureeSemiBold14)
                .foregroundColor(color)
                .lineLimit(1)
            Spacer()
            }
            HStack {
                if let image = image {
                    image
                }
            }.padding()
        }
        .frame(maxWidth: .infinity, minHeight: 54, idealHeight: 54, maxHeight: 54, alignment: .leading)
        .fixedSize(horizontal: false, vertical: true)
    }
}

struct StickyHeader_Previews: PreviewProvider {
    static var previews: some View {
        StickyHeaderView(backImage: Image("close-glyph"), title: "Enter serial number", image: nil, color: Color.indigo, onBack: {})
    }
}
