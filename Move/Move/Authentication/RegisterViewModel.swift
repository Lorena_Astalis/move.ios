//
//  RegisterViewModel.swift
//  Move
//
//  Created by Lorena Astalis on 13.04.2021.
//

import Foundation

class RegisterViewModel: ObservableObject {
    @Published var email: String
    @Published var username: String
    @Published var password: String
    
    init() {
        email = ""
        username = ""
        password = ""
    }
    
    func register(callbackSuccess: @escaping () -> Void, callbackFailure: @escaping () -> Void) {
        API.register(username: self.username, email: self.email, password: self.password) { (result) in
            switch result {
            case .success(let authResult):
                Session.shared.authToken = authResult.token
                callbackSuccess()
            case .failure(let error):
                print(error.localizedDescription)
                callbackFailure()
            }
        }
    }
    
    func validate() throws {
        try UserRegisterValidator.validation(email: self.email, password: self.password)
    }
}
