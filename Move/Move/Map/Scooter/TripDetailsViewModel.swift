//
//  TripDetailesViewModel.swift
//  Move
//
//  Created by Lorena Astalis on 12.05.2021.
//

import Foundation
import CoreLocation

class TripDetailsViewModel: ObservableObject {
    @Published var locationManager = LocationManager()
    @Published var booking: Booking
    @Published var totalDistance: Double
    @Published var scooter: Scooter?
    @Published var time = 0
    var endAddress: String = ""
    var endRide = false
    
    init(booking: Booking) {
        self.booking = booking
        self.totalDistance = 0
        self.loadData()
        self.getScooter()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            self.getLocation()
        })
    }
    
    func getScooter() {
        API.getScooterById(scooterId: booking.booking.scooterId, { result in
            switch result {
            case .success(let scooter):
                self.scooter = scooter.scooter
                break
            case.failure:
                break
            }
        })
    }
    
    func onLockUnlock() {
        API.lockUnlockScooter(bookingId: booking.booking.id, { result in
            switch result {
            case .success(let scooter):
                self.scooter = Scooter(id: scooter.scooterStatus.id, battery: scooter.scooterStatus.battery, location: scooter.scooterStatus.location, locked: scooter.scooterStatus.locked, internalId: scooter.scooterStatus.internalId, uniqueNumber: scooter.scooterStatus.uniqueNumber)
                print("Success")
                break
            case .failure(let error):
                showError(error: error.localizedDescription)
                break
            }
        })
    }
    
    func refreshData() {
        getLocation()
        API.getCurrentBooking { result in
            switch result {
            case .success(let booking):
                self.booking = booking
                self.endRide = false
                if let battery = booking.booking.scooterBattery {
                    self.scooter?.battery = battery
                }
                self.totalDistance = booking.booking.distance
                self.time = self.getSeconds(string: booking.booking.startDate)
                break
            case .failure:
                self.endRide = true
                break
            }
        }
    }
    
    func loadData() {
        print("Loading data ...")
        self.refreshData()
        print(endAddress)
        DispatchQueue.main.asyncAfter(deadline: .now() + 30) {
            if !self.endRide {
                self.loadData()
            }
        }
    }
    
    func endRide(_ callback: @escaping (Result<BookingComplete>) -> Void) {
        getLocation()
        guard let location = locationManager.location?.coordinate else { return }
        endRide = true
        API.endRide(long: location.longitude, lat: location.latitude, address: endAddress) { result in
            callback(result)
        }
    }
    
    func getLocation() {
        guard let location = locationManager.location?.coordinate else {
            return
        }
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(CLLocation(latitude: location.latitude, longitude: location.longitude),
            completionHandler: { (placemarks, error) in
                guard let placemark = placemarks?.last else {
                    return
                }
                print(placemark)
                if let street = placemark.thoroughfare {
                    self.endAddress = street
                }
                
                if let number = placemark.subThoroughfare {
                    self.endAddress += ", \(number)"
                }
            })
    }
    
    func getSeconds(string: String) -> Int {
        // formatting date
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        formatter.timeZone = TimeZone(abbreviation: "EEST")

        // convert string to date
        let startDate: Date = formatter.date(from: string)!
        // let startDatePlusOffset = TimeZone.current.secondsFromGMT() - 2*60*60

        let startDateInterval = startDate.timeIntervalSince1970
        let currentDate = Date()
        let currentDateInterval = currentDate.timeIntervalSince1970
        let intInterval = Int( currentDateInterval - startDateInterval)
        return intInterval
    }
}
