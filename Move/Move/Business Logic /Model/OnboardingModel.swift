//
//  OnboardingModel.swift
//  Move
//
//  Created by Lorena Astalis on 07.04.2021.
//

import SwiftUI

struct OnboardingData {
    let title: String
    let text: String
    let image: String
}

var onboardingData = [
    OnboardingData(title: "Safety", text: "Please wear a helmet and protect yourself while riding.", image: "safety-glyph"),
    OnboardingData(title: "Scan", text: "Scan the QR code or NFC sticker on top of the scooter to unlock and ride.", image: "scan-glyph"),
    OnboardingData(title: "Ride", text: "If convenient, park at a bike rack. If not, park close to the edge of the sidewalk closest to the street. Do not block sidewalks, doors or ramps.", image: "ride-glyph"),
    OnboardingData(title: "Parking", text: "If convenient, park at a bike rack. If not, park close to the edge of the sidewalk closest to the street. Do not block sidewalks, doors or ramps.", image: "parking-glyph"),
    OnboardingData(title: "Rules", text: "You must be 18 years or and older with a valid driving licence to operate a scooter. Please follow all street signs, signals and markings, and obey local traffic laws.", image: "rules-glyph")]
