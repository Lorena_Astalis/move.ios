//
//  PingScooterModel.swift
//  Move
//
//  Created by Lorena Astalis on 13.05.2021.
//

import Foundation

class PingScooter: Codable {
    let pinged: Bool
    
    enum CodingKeys: String, CodingKey {
        case pinged = "pinged"
    }
}
