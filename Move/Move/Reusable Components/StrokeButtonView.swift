//
//  StrokeButtonView.swift
//  Move
//
//  Created by Lorena Astalis on 27.04.2021.
//

import SwiftUI

struct StrokeButtonView: View {
    let image: Image?
    let text: String
    let color: Color
    let pressed: () -> Void
    
    var body: some View {
        Button {
            pressed()
        } label: {
            ZStack {
                RoundedRectangle(cornerRadius: 16)
                    .fill(Color.white.opacity(0.2))
                    .overlay (
                        RoundedRectangle(cornerRadius: 16)
                            .stroke(color, lineWidth: 1)
                    )
                HStack {
                if let image = image {
                    image
                }
                Text(text)
                    .font(.baiJamjureeBold16)
                    .foregroundColor(color)
                }
            }
        }
        .frame(height: 56)
        .frame(maxWidth: .infinity)
    }
}

struct StrokeButtonView_Previews: PreviewProvider {
    static var previews: some View {
        StrokeButtonView(image: nil, text: "NFC", color: Color.raspberry, pressed: {})
    }
}
