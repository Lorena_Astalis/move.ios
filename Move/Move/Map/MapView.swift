//
//  MapView.swift
//  Move
//
//  Created by Lorena Astalis on 16.04.2021.
//

import SwiftUI
import MapKit
import UIKit
import Combine

struct MapView: View {
    @ObservedObject var locationManager: LocationManager
    @State private var region: MKCoordinateRegion = MKCoordinateRegion.defaultRegion
    @State private var viewModel: MapViewModel
    @State var scootersToShow: [Scooter]? = nil
    @State var scooterToUnlock: Scooter? = nil
    let onMenu: () -> Void
    let onShowScooters: ([Scooter]) -> Void
    
    init(locationManager: LocationManager, onMenu: @escaping () -> Void, onShowScooters: @escaping ([Scooter]) -> Void) {
        let viewModel = MapViewModel(locationManager: locationManager)
        self.locationManager = locationManager
        self.viewModel = viewModel
        if let location = locationManager.location {
            locationManager.getCityName(location: location)
        }
        self.onMenu = onMenu
        self.onShowScooters = onShowScooters
    }

    var body: some View {
        ZStack(alignment: .top) {
            Map(coordinateRegion: $region, interactionModes: .all, showsUserLocation: true, annotationItems: viewModel.scooters) { scooter in
                MapAnnotation(coordinate: CLLocationCoordinate2D(latitude: scooter.location.coordinates[1], longitude: scooter.location.coordinates[0])) {
                        Image("map-pin-glyph")
                            .onTapGesture {
                                onShowScooters([scooter])
                            }
                }
            }
            .ignoresSafeArea()
      
            Image("map-gradient-glyph")
                .resizable()
                .ignoresSafeArea()
                .scaledToFit()
                .padding(.top, -80)
                
            HStack {
                RoundedButtonView(image: Image("menu-lines-glyph"), border: false, action: {
                    onMenu()
                })
                    .padding(.horizontal, 24)
                Spacer()
                Text(locationManager.city)
                        .font(.baiJamjureeSemiBold17)
                        .foregroundColor(.indigo)
                Spacer()
                if let _ = locationManager.location {
                    RoundedButtonView(image: Image("location-glyph"), border: false, action: {
                        setCurrentLocation()
                    })
                    .padding(.horizontal, 24)
                        
                } else {
                    RoundedButtonView(image: Image("no-location-glyph"), border: false, action: {
                        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                                   return
                        }
                        UIApplication.shared.open(settingsUrl, completionHandler: { success in
                                            print("Settings opened: \(success)")
                        })
                    })
                    .padding(.horizontal, 24)
                }
            }
        }
        .statusBarStyle(.darkContent)
        .environmentObject(self.locationManager)
        .onAppear {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                setCurrentLocation()
            }
        }
        
    }
    
    private func setCurrentLocation() {
        if let loc = locationManager.location {
            region = MKCoordinateRegion(center: loc.coordinate , latitudinalMeters: 200, longitudinalMeters: 200)
        }
    }
}

struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        MapView(locationManager: LocationManager(), onMenu: {}, onShowScooters: {_  in })
    }
}
