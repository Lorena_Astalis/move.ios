//
//  MenuBackgroundView.swift
//  Move
//
//  Created by Lorena Astalis on 22.04.2021.
//

import SwiftUI

struct MenuBackgroundView: View {
    var body: some View {
        ZStack(alignment: .bottomTrailing) {
            Color.white
            Image("menu-scooter-glyph")
                .resizable()
                .scaledToFit()
        }
    }
}

struct MenuBackgroundView_Previews: PreviewProvider {
    static var previews: some View {
        MenuBackgroundView()
    }
}
