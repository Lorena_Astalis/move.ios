//
//  MenuViewModel.swift
//  Move
//
//  Created by Lorena Astalis on 17.05.2021.
//

import Foundation


class MenuViewModel: ObservableObject {
    @Published var user: User?
    
    init() {
        API.getCurrentUser({ result in
            switch result {
            case .success(let user):
                self.user = user
                break
            case .failure(let error):
                showError(error: error.localizedDescription)
                break
            }
            
        })
    }
}
