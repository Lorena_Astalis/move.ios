//
//  VerifyingLicense.swift
//  Move
//
//  Created by Lorena Astalis on 15.04.2021.
//

import SwiftUI

struct VerifyingLicenseView: View {
    
    let exploreTheApp: () -> Void
    
    var body: some View {
        ZStack(alignment: .center) {
            BackgroundView()
                .ignoresSafeArea(.all)
            VStack(alignment: .center) {
                Spacer()
                Text("We are currently verifying your driving license")
                    .font(.baiJamjureeBold32)
                    .foregroundColor(.white)
                    .multilineTextAlignment(.center)
                    .padding(28)
                Text("Check back shortly and if everything\nlooks good you’ll be riding our scooters in\nno time.")
                    .font(.baiJamjureeMedium16)
                    .foregroundColor(.cloudy)
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, 28)
                Spacer()
                exploreButton
            }
        }
    }
    
    var exploreButton: some View {
        Button {
            exploreTheApp()
        } label: {
            Text("Explore the app")
                .font(.baiJamjureeMedium16)
                .foregroundColor(.white)
                .frame(height: 56)
                .frame(maxWidth: .infinity)
                .background(RoundedRectangle(cornerRadius: 16)
                .fill(Color.raspberry))
                .padding(4)
        }
        .padding(24)
    }
}

struct VerifyingLicense_Previews: PreviewProvider {
    static var previews: some View {
        VerifyingLicenseView(exploreTheApp: {})
    }
}
