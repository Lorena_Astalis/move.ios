//
//  Fonts.swift
//  Move
//
//  Created by Lorena Astalis on 08.04.2021.
//

import SwiftUI

extension Font {
    static var baiJamjureeRegular: Font {
        return Font.custom("BaiJamjuree-Regular", size: 32)
    }
}

