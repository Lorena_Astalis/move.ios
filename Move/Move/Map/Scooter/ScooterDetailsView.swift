//
//  ScooterDetailsView.swift
//  Move
//
//  Created by Lorena Astalis on 20.04.2021.
//

import SwiftUI
import MapKit

struct ScooterDetailsView: View {
    @State var street = ""
    @State var waiting = false
    let scooter: Scooter
    let booking: Booking?
    let onPingScooter: () -> Void
    let onUnlock: (Scooter) -> Void
    
    var body: some View {
        ZStack {
            background
            VStack {
                HStack(spacing: 0) {
                    Image("scooter-details-glyph")
                    detailes
                }.padding(.trailing, 7)
                Spacer()
                location
                ButtonView(text: "Unlock", activateButton: true, callback: {
                    onUnlock(scooter)
                }, image: nil, color: .white, waiting: waiting)
                    .frame(width: 202, height: 56)
                    .padding(.vertical, 24)
            }
        }
        .frame(width: 250, height: 315)
        .shadow(color: Color.cloudy.opacity(0.15), radius: 20, x: 7, y: 7)
        .onAppear {
            getLocation()
        }
    }
    
    func getLocation() {
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(CLLocation(latitude: scooter.location.coordinates[1], longitude: scooter.location.coordinates[0]),
            completionHandler: { (placemarks, error) in
                guard let placemark = placemarks?.last else {
                    return
                }
                print(placemark)
                if let street = placemark.thoroughfare {
                    self.street = street
                }
                
                if let number = placemark.subThoroughfare {
                    self.street += ", \(number)"
                }
            })
    }
            
    var location: some View {
        HStack {
            Image("address-pin-glyph")
            Text("\(street)")
                .font(.baiJamjureeMedium14)
                .foregroundColor(.indigo)
        }
    }
    
    var detailes: some View {
        VStack(alignment: .trailing, spacing: 5) {
            Spacer()
            Text("Scooter")
                .font(.baiJamjureeMedium14)
                .foregroundColor(.indigo)
                .opacity(0.6)
            Text("#\(scooter.uniqueNumber)")
                .font(.baiJamjureeBold20)
                .foregroundColor(.indigo)
            BatteryView(batteryLevel: scooter.battery, expanded: false)
            Spacer()
            HStack(spacing: 15) {
                RoundedButtonView(image: Image("bell-glyph"), border: false, action: {
                    onPingScooter()
                })
                RoundedButtonView(image: Image("navigation-glyph"), border: false, action: {})
            }
        }
    }
    
    var background: some View {
        GeometryReader { _ in
            RoundedRectangle(cornerRadius: 29)
                .fill(Color.white)
                .frame(width: 250, height: 315)
            RoundedRectangle(cornerRadius: 59)
                .fill(Color.cloudy)
                .opacity(0.15)
                .frame(width: 152, height: 152)
                .rotationEffect(Angle(degrees: -129))
                .padding(.horizontal, -35)
                .padding(.vertical, -60)
        }
        .cornerRadius(29)
        .clipped()
    }
}

struct ScooterDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        ScooterDetailsView(scooter: Scooter(id: "123gfrtrtyfggyr4", battery: 89, location: Location(type: "Point", coordinates: [ 23.591423, 46.770439]), locked: true, internalId: "32453423", uniqueNumber: "4324"), booking: nil, onPingScooter: {}, onUnlock: { _ in })
    }
}
