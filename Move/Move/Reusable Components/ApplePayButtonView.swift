//
//  ApplePayButtonView.swift
//  Move
//
//  Created by Lorena Astalis on 12.05.2021.
//

import SwiftUI

struct ApplePayButtonView: View {
    let callback: () -> Void
    
    var body: some View {
        Button {
            callback()
        } label: {
            RoundedRectangle(cornerRadius: 16)
                .fill(Color.black)
                .overlay(
                    Text("Pay with Pay")
                        .font(.system(size: 20))
                        .foregroundColor(.white)
                )
                .frame(height: 56)
        }
    }
}

struct ApplePayButtonView_Previews: PreviewProvider {
    static var previews: some View {
        ApplePayButtonView(callback: {})
    }
}
