//
//  LoginView.swift
//  Move
//
//  Created by Lorena Astalis on 15.04.2021.
//

import SwiftUI

struct LoginView: View {
    @ObservedObject var loginViewModel = LoginViewModel()
    @State var isFocused = [false, false]
    @State var emailError = false
    @State var passwordError = false
    @State var errorMessage = ""
    @State var waiting = false
    let onForgotPassword: () -> Void
    let noAccount: () -> Void
    let onNext: () -> Void
    
    var body: some View {
        ZStack(alignment: .center) {
            BackgroundView()
                .edgesIgnoringSafeArea(.all)
            ScrollView {
                VStack(alignment: .leading) {
                    Icon()
                    Text("Login")
                        .font(.baiJamjureeBold32)
                        .foregroundColor(.white)
                        .padding(.vertical, 20)
                    Text("Enter your account credentials\nand start riding away")
                        .font(.baiJamjureeMedium20)
                        .fixedSize(horizontal: false, vertical: true)
                        .foregroundColor(.white)
                        .opacity(0.6)
                    textFields
                        .padding(.vertical, 10)
                    forgotPassword
                        .padding(.vertical, 10)
                    getStartedButton
                    dontHaveAnAccount
                        .frame(alignment: .center)
                        .padding(.vertical, 10)
                }
                .padding(24)
            }
        }
    }
    
    var textFields: some View {
        VStack {
            CustomTextFieldView(id: 0, text: $loginViewModel.email, image: [Image("close-circle")], placeholder: "Email address", isSecured: false, isFocused: $isFocused, underComment: nil, error: emailError, errorMessage: nil, color: Color.white)
            CustomTextFieldView(id: 1, text: $loginViewModel.password, image: [Image("open-eye-glyph"), Image("closed-eye-glyph")], placeholder: "Password", isSecured: true, isFocused: $isFocused, underComment: nil, error: passwordError, errorMessage: errorMessage, color: Color.white)
        }
    }
    
    func reset() {
        emailError = false
        passwordError = false
        errorMessage = ""
    }
    
    var getStartedButton: some View {
        ButtonView(text: "Get started",
           activateButton: !(loginViewModel.email.isEmpty ||
                loginViewModel.password.isEmpty),
           callback: {
            waiting = true
                reset()
                loginViewModel.login(success: {
                    onNext()
                }, failure: {
                    emailError = true
                    passwordError = true
                    errorMessage = "Email or password incorrect"
                    waiting = false
                })
           }, image: nil, color: .white, waiting: waiting)
    }
    
    var forgotPassword: some View {
        Button {
            onForgotPassword()
        } label: {
            Text("Forgot your password?")
                .font(.baiJamjureeRegular12)
                .foregroundColor(.white)
                .underline()
        }
    }
    
    var dontHaveAnAccount: some View {
        HStack(spacing: 3) {
            Text("Don’t have an account? You can")
                .foregroundColor(.white)
                .font(.baiJamjureeRegular12)
            Button {
                noAccount()
            } label: {
                Text("start with one here")
                    .foregroundColor(.white)
                    .font(.baiJamjureeSemiBold12)
                    .underline()
            }
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView(onForgotPassword: {}, noAccount: {}, onNext: {})
    }
}
