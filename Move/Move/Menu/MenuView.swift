//
//  MenuView.swift
//  Move
//
//  Created by Lorena Astalis on 22.04.2021.
//

import SwiftUI
import BetterSafariView

struct MenuView: View {
    @ObservedObject var viewModel = MenuViewModel()
    @State var waiting = false
    let onBack: () -> Void
    let onSeeHistory: () -> Void
    let onAccount: (String, String) -> Void
    let onChangePassword: () -> Void
    
    var body: some View {
        ZStack {
            MenuBackgroundView()
            VStack {
                StickyHeaderView(backImage: Image("arrow-left-glyph"), title: "Hi, \(viewModel.user?.username ?? "No data avaiable")!", image: Image("girl-profile-glyph"), color: .indigo, onBack: {
                    onBack()
                })
                ScrollView {
                    VStack(alignment: .leading) {
                        historySection
                        generalSettings
                        legacy
                        rating
                        Spacer()
                    }
                    .padding(.horizontal, 24)
                }
            }
        }
    }
    
    var generalSettings: some View {
        VStack {
            HStack(alignment: .top, spacing: 16) {
                Image("nut-glyph")
                    .padding(.bottom, 16)
                VStack(alignment: .leading) {
                    Text("General settings")
                        .font(.baiJamjureeBold18)
                        .foregroundColor(.indigo)
                        .padding(.bottom, 16)
                    Button {
                        onAccount(viewModel.user?.username ?? "", viewModel.user?.id ?? "")
                    } label: {
                        Text("Account")
                            .font(.baiJamjureeMedium14)
                            .foregroundColor(.indigo)
                    }
                    .padding(.bottom, 32)
                    Button {
                        onChangePassword()
                    } label: {
                        Text("Change password")
                            .font(.baiJamjureeMedium14)
                            .foregroundColor(.indigo)
                    }
                    .padding(.bottom, 32)
                }
            }
        }
    }
    
    @State var isPresented = false
    let url = URL(string: "https://tapptitude.com")
    var legacy: some View {
        VStack {
            HStack(alignment: .top, spacing: 16) {
                Image("flag-glyph")
                    .padding(.bottom, 16)
                VStack(alignment: .leading) {
                    Text("Legacy")
                        .font(.baiJamjureeBold18)
                        .foregroundColor(.indigo)
                        .padding(.bottom, 16)
                    Button {
                        isPresented = true
                    } label: {
                        Text("Terms and Conditions")
                            .font(.baiJamjureeMedium14)
                            .foregroundColor(.indigo)
                    }
                    .safariView(isPresented: $isPresented) {
                        SafariView(url: url!,
                                   configuration: SafariView.Configuration(entersReaderIfAvailable: false, barCollapsingEnabled: true))
                    }
                    .padding(.bottom, 32)
                    
                    Button {
                        isPresented = true
                    } label: {
                        Text("Privacy Policy")
                            .font(.baiJamjureeMedium14)
                            .foregroundColor(.indigo)
                    }
                    .safariView(isPresented: $isPresented) {
                        SafariView(url: url!,
                                   configuration: SafariView.Configuration(entersReaderIfAvailable: false, barCollapsingEnabled: true))
                    }
                    .padding(.bottom, 32)
                }
            }
        }
    }
    
    var rating: some View {
        Button {
            
        } label: {
            HStack(alignment: .top, spacing: 16) {
                Image("star-glyph")
                    .padding(.bottom, 32)
                Text("Rate us")
                    .font(.baiJamjureeBold18)
                    .foregroundColor(.indigo)
                    .padding(.bottom, 32)
            }
        }
    }
    
    var historySection: some View {
        ZStack {
            historyBackground
            HStack(alignment: .top) {
                Spacer()
                VStack(alignment: .leading) {
                    Text("History")
                        .font(.baiJamjureeBold16)
                        .foregroundColor(.white)
                    Text("Total rides: \(viewModel.user?.noTrips ?? 0)")
                        .font(.baiJamjureeMedium16)
                        .foregroundColor(.cloudy)
                }.padding(.vertical, 24)
                Spacer()
                ButtonView(text: "See all", activateButton: true, callback: {
                    waiting = true
                    onSeeHistory()
                }, image: Image("right-arrow-glyph"), color: .white, waiting: waiting)
                .frame(width: 111, height: 48)
                .padding(.vertical, 24)
                Spacer()
            }
        }
        .shadow(color: Color.cloudy.opacity(0.15), radius: 20, x: 7, y: 7)
        .padding(.bottom, 32)
    }
    
    var historyBackground: some View {
        GeometryReader { _ in
            RoundedRectangle(cornerRadius: 29)
                .fill(Color.deepPurple)
            RoundedRectangle(cornerRadius: 60)
                .fill(Color.white.opacity(0.05))
                .rotationEffect(Angle(degrees: 135))
                .frame(width: 153, height: 153)
                .padding(.horizontal, -5)
                .padding(.vertical, -40)
        }
        .frame(maxWidth: .infinity, maxHeight: 112)
        
    }
}

struct MenuView_Previews: PreviewProvider {
    static var previews: some View {
        MenuView(onBack: {},onSeeHistory: {}, onAccount: {_,_ in }, onChangePassword: {})
    }
}
