//
//  Register.swift
//  Move
//
//  Created by Lorena Astalis on 08.04.2021.
//

import SwiftUI
import BetterSafariView

struct RegisterView: View {
    @ObservedObject var registerViewModel = RegisterViewModel()
    @State var emailError = false
    @State var passwordError = false
    @State var isFocused = [false, false, false]
    @State var errorMessage = ""
    @State var waiting = false
    let onFinished: () -> Void
    let onLogin: ()-> Void
   
    
    var body: some View {
        ZStack(alignment: .center) {
            BackgroundView()
                .edgesIgnoringSafeArea(.all)
            ScrollView {
                VStack(alignment: .leading) {
                        Icon()
                        Text("Let's get started")
                            .font(.baiJamjureeBold32)
                            .foregroundColor(.white)
                            .padding(.vertical, 20)
                        Text("Sign up or login and start\nriding right away")
                            .font(.baiJamjureeMedium20)
                            .fixedSize(horizontal: false, vertical: true)
                            .foregroundColor(.white)
                            .opacity(0.6)
                        textFields
                            .padding(.vertical, 10)
                        TermsAndConditions()
                        getStartedButton
                        AlreadyHaveAnAccount(onLogin: { onLogin() })
                    }
                    .padding(24)
            }
        }
    }

    func reset() {
        errorMessage = ""
        emailError = false
        passwordError = false
    }
    
    var getStartedButton: some View {
        ButtonView(text: "Get started",
           activateButton: !(registerViewModel.email.isEmpty || registerViewModel.username.isEmpty || registerViewModel.password.isEmpty),
           callback: {
            waiting = true
                do {
                    reset()
                    try registerViewModel.validate()
                } catch let error as UserRegisterError {
                    waiting = false
                    switch error {
                    case .invalidEmail:
                        emailError = true
                        errorMessage = error.description
                        return
                    case .invalidPassword:
                        passwordError = true
                        errorMessage = error.description
                        return
                    }
                } catch {
                    waiting = false
                }
                registerViewModel.register(callbackSuccess: {
                        onFinished()
                    waiting = false
                }
                , callbackFailure: {
                    emailError = true
                    errorMessage = "Email already taken"
                    waiting = false
                })
           }, image: nil, color: .white, waiting: waiting)
    }
    
    var textFields: some View {
        VStack {
            CustomTextFieldView(id: 0, text: $registerViewModel.email, image: [Image("close-circle")], placeholder: "Email address", isSecured: false, isFocused: $isFocused, underComment: nil, error: emailError, errorMessage: errorMessage, color: .white)
            CustomTextFieldView(id: 1, text: $registerViewModel.username, image: nil, placeholder: "Username", isSecured: false, isFocused: $isFocused, underComment: nil, error: false, errorMessage: nil, color: .white)
            CustomTextFieldView(id: 2, text: $registerViewModel.password, image: [Image("open-eye-glyph"), Image("closed-eye-glyph")], placeholder: "Password", isSecured: true, isFocused: $isFocused, underComment: "Use a strong password (min. 8 characters and use symbols)", error: passwordError, errorMessage: errorMessage, color: .white)
        }
    }
}

struct Icon: View {
    var body: some View {
        ZStack {
            Rectangle()
                .fill(Color.white)
                .frame(width: 56, height: 56)
                .cornerRadius(22)
                .rotationEffect(Angle(degrees: 45))
            ZStack(alignment: .top) {
                Image("pin-glyph")
                Circle()
                    .fill(Color.white)
                    .frame(width: 14, height: 14)
                    .padding(6)
            }
            Image("logo-glyph")
        }.padding(4)
    }
}


struct AlreadyHaveAnAccount: View {
    let onLogin: ()-> Void
    var body: some View {
        HStack(spacing: 3) {
            Text("You already have an account? You can")
                .font(.baiJamjureeRegular12)
                .foregroundColor(.white)
            Button {
                onLogin()
            } label: {
                Text("log in here")
                .font(.baiJamjureeSemiBold12)
                .underline()
                .foregroundColor(.white)
                .fixedSize(horizontal: false, vertical: true)
            }
        }
    }
}

struct TermsAndConditions: View {
    @State var isPresented = false
    let url = URL(string: "https://tapptitude.com")
    var body: some View {
        VStack(alignment: .leading) {
            Text("By continuing you agree to Move’s")
                .font(.baiJamjureeRegular12)
            HStack(spacing: 3) {
                Button {
                    isPresented = true
                } label: {
                    Text("Terms and Conditions")
                        .font(.baiJamjureeSemiBold12)
                        .underline()
                }
                .safariView(isPresented: $isPresented) {
                    SafariView(url: url!,
                               configuration: SafariView.Configuration(entersReaderIfAvailable: false, barCollapsingEnabled: false))
                }
                Text("and")
                    .font(.baiJamjureeRegular12)
                Button {
                    isPresented = true
                } label: {
                    Text("Privacy Policy")
                        .font(.baiJamjureeSemiBold12)
                        .underline()
                }
                .safariView(isPresented: $isPresented) {
                    SafariView(url: url!,
                               configuration: SafariView.Configuration(entersReaderIfAvailable: false, barCollapsingEnabled: false))
                }
                
            }
        }
        .foregroundColor(.white)
    }
}


struct Register_Previews: PreviewProvider {
    static var previews: some View {
        RegisterView(onFinished: {}, onLogin: {})
    }
}
