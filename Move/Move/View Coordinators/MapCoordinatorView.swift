//
//  MapCoordinatorView.swift
//  Move
//
//  Created by Lorena Astalis on 07.05.2021.
//

import SwiftUI
import NavigationStack

struct MapCoordinatorView: View {
    @StateObject var viewModel = MapCoordinatorViewModel()
    let navigationViewModel: NavigationStack
    let onMenu: () -> Void
    
    var body: some View {
        MapView(locationManager: viewModel.locationManager,
        onMenu: {
            onMenu()
        }, onShowScooters: {scooters  in
            viewModel.scooterToUnlock = scooters[0]
            if viewModel.overlay == Overlays.tripDetails { return }
            viewModel.overlay = .unlockMethods
        })
        .onAppear {
            viewModel.getCurrentBooking()
        }
        .overlay(handleOverlay(type: viewModel.overlay))
        .onReceive(Session.shared.objectWillChange, perform: { _ in
                    navigationViewModel.pop(to: .root)
        })
    }
    
    func handleOverlay(type: Overlays) -> AnyView {
        if viewModel.overlay == Overlays.tripDetails { return AnyView(handleTripDetails()) }
        switch type {
        case .scooters:
            guard let location = viewModel.locationManager.location?.coordinate else { return AnyView(EmptyView()) }
            guard let scooters = viewModel.scooters else { return AnyView(EmptyView()) }
            return AnyView(showScooters(location: [location.latitude, location.longitude], scooters: scooters))
        case .unlockMethods:
            guard let location = viewModel.locationManager.location?.coordinate else { return AnyView(EmptyView()) }
            guard let scooter = viewModel.scooterToUnlock else { return AnyView(EmptyView()) }
            return AnyView(showUnlockTypes(location: [location.latitude, location.longitude], scooter: scooter))
        case .tripDetails:
            return AnyView(handleTripDetails())
        case .none: return AnyView(EmptyView())
        }
    }
    
    func showScooters(location: [Double], scooters: [Scooter]) -> some View {
        VStack {
            Spacer()
            ShowScootersDetailsView(scooters: scooters, location: location, onUnlock: { scooter in
                if viewModel.overlay == Overlays.tripDetails { return }
                viewModel.reset()
                viewModel.scooterToUnlock = scooter
                viewModel.overlay = Overlays.unlockMethods
            })
            .padding(.bottom, 46)
        }
    }
    
    func showUnlockTypes(location: [Double], scooter: Scooter) -> some View {
        ScooterUnlockView(scooter: scooter, currentLocation: location, dragDown: {
            if viewModel.overlay == Overlays.tripDetails { return }
            viewModel.overlay = Overlays.none
        }, onUnlock: { type in
            handleUnlock(type: type)
        })
    }
    
    func handleTripDetails() -> AnyView {
        guard let booking = viewModel.booking else { return AnyView(EmptyView()) }
        return AnyView(TripDetailsView(booking: booking, onLock: {
            viewModel.lockScooter()
        }, onEndTrip: { ride in
            viewModel.reset()
            navigationViewModel.push(TripSummaryView(ride: ride, onNext: {
                navigationViewModel.pop(to: .view(withId: "map"))
            }))
        }))
    }
    
    func handleUnlock(type: UnlockType) {
        guard let location = viewModel.locationManager.location else { return }
        guard let scooter = viewModel.scooterToUnlock else { return }
        switch type {
        case .code:
            handleCodeUnlockScooter(onNext: { booking in
                viewModel.scooter = viewModel.scooterToUnlock
                viewModel.booking = booking
                viewModel.overlay = Overlays.tripDetails
                handleSuccessfulUnlock()
            })
        case .qr:
            handleQrUnlockScooter(scooter: scooter,
                location: [location.coordinate.latitude, location.coordinate.longitude],
                onNext: { booking in
                viewModel.scooter = viewModel.scooterToUnlock
                viewModel.booking = booking
                viewModel.overlay = Overlays.tripDetails
                handleSuccessfulUnlock()
            })
            break
        case .nfc:
            handleNfcUnlockScooter(onNext: { booking in
                viewModel.scooter = viewModel.scooterToUnlock
                viewModel.booking = booking
                viewModel.overlay = Overlays.tripDetails
                handleSuccessfulUnlock()
            })
            break
        }
    }
    
    func handleCodeUnlockScooter(onNext: @escaping (Booking) -> Void) {
        guard let scooter = viewModel.scooterToUnlock else { return }
        guard let location = viewModel.locationManager.location?.coordinate else { return }
        navigationViewModel.pop(to: .view(withId: "map"))
        navigationViewModel.push(
            CodeUnlockView(
                scooter: scooter,
                location: [location.latitude, location.longitude],
                onClose: {
                    navigationViewModel.pop()
                }
                ,onStartRide: onNext
                ,onOtherUnlockType: { type in
                    navigationViewModel.pop()
                    self.handleUnlock(type: type)
                })
        )
    }
    
    func handleNfcUnlockScooter(onNext: @escaping (Booking) -> Void) {
        guard let location = viewModel.locationManager.location?.coordinate else { return }
        navigationViewModel.pop(to: .view(withId: "map"))
        navigationViewModel.push(NfcUnlockView(location: [location.latitude, location.longitude],onClose: {
            navigationViewModel.pop()
        }, onOtherUnlockType: { type in
            handleUnlock(type: type)
        }, onNext: onNext))
    }
    
    func handleQrUnlockScooter(scooter: Scooter, location: [Double], onNext: @escaping (Booking) -> Void) {
        navigationViewModel.pop(to: .view(withId: "map"))
        navigationViewModel.push(QrUnlockView(scooter: scooter, location: location, onClose: {
            navigationViewModel.pop()
        }, onOtherUnlockType: { type in
            handleUnlock(type: type)
        }, onNext: onNext))
    }
    
    func handleSuccessfulUnlock() {
        navigationViewModel.push(UnlockSuccessfulView())
        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
            navigationViewModel.pop(to: .view(withId: "map"))
        })
    }
}
