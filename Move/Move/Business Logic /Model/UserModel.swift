//
//  UserModel.swift
//  Move
//
//  Created by Lorena Astalis on 09.04.2021.
//

import Foundation

struct User: Codable {
    let id: String
    let noTrips: Int
    let username: String
    
    enum CodingKeys: String, CodingKey {
        case noTrips = "noTrips"
        case id = "email"
        case username = "username"
    }
}
