//
//  DriverLicenseValidation.swift
//  Move
//
//  Created by Lorena Astalis on 15.04.2021.
//

import SwiftUI

struct DriverLicenseValidationView: View {
    @State var addLicense = false
    @State var camera = false
    @State var picker = false
    @State var presentActionSheet = false
    @State var waiting = false
    let onBack: () -> Void
    let onNext: (Image) -> Void
    
    var body: some View {
        VStack {
            StickyHeaderView(backImage: Image("arrow-left-glyph"),
                             title: "Add License", image: nil, color: .indigo, onBack: {
                                onBack()
                             })
            ScrollView {
                VStack(alignment: .leading, spacing: 4) {
                    Image("driver-license-glyph")
                        .resizable()
                        .scaledToFill()
                    Spacer()
                    Text("Before you can start riding")
                        .font(.baiJamjureeBold32)
                        .foregroundColor(.indigo)
                        .padding(.horizontal, 24)
                    Spacer()
                    Text("Please take a photo or upload the front side of your driving license so we can make sure that it is valid.")
                        .font(.baiJamjureeMedium16)
                        .foregroundColor(.indigo)
                        .opacity(0.7)
                        .padding(.horizontal, 24)
                    Spacer()
                }
            }
            addLicenseButton
        }
        .statusBarStyle(.darkContent)
    }
    
    var addLicenseButton: some View {
        ButtonView(text: "Add License", activateButton: true, callback: {
            handleImagePicker()
        }, image: nil, color: Color.white, waiting: waiting)
        .sheet(isPresented: $picker) {
            SUImagePickerView(sourceType: .photoLibrary,
                              image: imageBinding, isPresented: $picker)
        }
        .sheet(isPresented: $camera) {
            ScanDocumentView(isPresented: $camera, image: imageBinding)
        }
        .actionSheet(isPresented: $presentActionSheet) {
            actionSheet
        }
        .padding([.horizontal, .bottom], 24)
    }
    
    func handleImagePicker() {
        presentActionSheet = true
    }
    
    var imageBinding: Binding<Image?> {
        return Binding(get: {
            Image("")
        }, set: { newImage in
            if let image = newImage {
                waiting = true
                onNext(image)
            }
        })
    }
    
    var actionSheet: ActionSheet {
        ActionSheet(title: Text("Choose mode"),
                message: Text("Please choose preffered mode to upload driver license"),
                buttons: [ActionSheet.Button.default(Text("Camera"), action: {
                                self.picker = false
                                self.camera = true
                            }), ActionSheet.Button.default(Text("Photo Library"), action: {
                                self.camera = false
                                self.picker = true
                            }), ActionSheet.Button.cancel({
                                showError(error: "Please upload your driver license")
                            })])
    }
}

struct DriverLicenseValidation_Previews: PreviewProvider {
    static var previews: some View {
        DriverLicenseValidationView(onBack: {}, onNext: { _ in })
    }
}
