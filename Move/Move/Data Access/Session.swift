//
//  Session.swift
//  Move
//
//  Created by Lorena Astalis on 12.04.2021.
//

import Foundation

class Session: ObservableObject {
    static var shared = Session()
    
    private init() {}
    
    var authToken: String? {
        get {
            UserDefaults.standard.string(forKey: "authToken")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "authToken")
        }
    }
    
    var isValidSession: Bool {
        return authToken != nil
    }
    
    var shouldInvalidateSession: Bool = false {
        didSet {
            self.objectWillChange.send()
        }
    }
}


