//
//  ScandDocumentView.swift
//  Move
//
//  Created by Lorena Astalis on 21.05.2021.
//

import SwiftUI
import VisionKit

struct ScanDocumentView: UIViewControllerRepresentable {
    @Binding var isPresented: Bool
    @Binding var image: Image?
        
    func makeCoordinator() -> Coordinator {
        Coordinator(parent: self, image: $image, isPresented: $isPresented)
    }

  
    func makeUIViewController(context: Context) -> VNDocumentCameraViewController {
        VNDocumentCameraViewController()
        
    }
    
    func updateUIViewController(_ uiViewController: VNDocumentCameraViewController, context: Context) {
                // nothing to do here
    }
    
}
