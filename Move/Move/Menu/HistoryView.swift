//
//  HistoryView.swift
//  Move
//
//  Created by Lorena Astalis on 05.05.2021.
//

import SwiftUI
import CoreLocation

struct HistoryView: View {
    @StateObject var viewModel = HistoryViewModel()
    let onBack: () -> Void
    
    var body: some View {
        VStack {
            StickyHeaderView(backImage: Image("arrow-left-glyph"), title: "History", image: nil, color: Color.indigo, onBack: {
                onBack()
            })
            ScrollView {
                PullToRefreshView(coordinateSpaceName: "refresh") {
                    viewModel.refresh()
                }
                VStack {
                    ForEach(viewModel.rides, id: \.id) { ride in
                        HistorySection(ride: ride)
                    }
                    if viewModel.rides.count >= 10 {
                        Button {
                            viewModel.loadMore()
                        } label: {
                            Text("Load more")
                                .font(.baiJamjureeBold16)
                                .foregroundColor(.cloudy)
                        }
                    }
                }
            }.coordinateSpace(name: "refresh")
        }
    }
}

struct HistorySection: View {
    let ride: Ride

    var body: some View {
        HStack(alignment: .alignTitles) {
                ZStack {
                    RoundedRectangle(cornerRadius: 29)
                        .fill(Color.cloudy.opacity(0.15))
                    HStack {
                        VStack(alignment: .leading, spacing: 8) {
                            displayText(title: "From", content: ride.startAddress ?? "Not available")
                                .lineLimit(.max)
                            displayText(title: "To", content: ride.endAddress ?? "Not available")
                            Spacer()
                        }
                        Spacer()
                    }
                    .padding()
                }
                VStack(alignment: .leading, spacing: 8) {
                    displayText(title: "Travel time", content: "\(ride.duration?.prefix(5) ?? "") min")
                    Spacer()
                    displayText(title: "Distance", content: String(format: "%.1f km", ride.distance ?? 0.0 as CVarArg))
                }.padding()
            }
            .overlay(
                RoundedRectangle(cornerRadius: 29)
                    .stroke(Color.indigo)
            )
            .padding(.horizontal, 24)
            .padding(.vertical, 6)
    }

    func displayText(title: String, content: String) -> some View {
        VStack(alignment: .leading) {
            Text(title)
                .font(.baiJamjureeMedium12)
                .foregroundColor(.gray)
            Text(content)
                .font(.baiJamjureeBold16)
                .foregroundColor(.indigo)
            
        }
    }
}

extension VerticalAlignment {
    struct AlignTitles: AlignmentID {
        static func defaultValue(in d: ViewDimensions) -> CGFloat {
            d[.top]
        }
    }

    static let alignTitles = VerticalAlignment(AlignTitles.self)
}

struct HistoryView_Previews: PreviewProvider {
    static var previews: some View {
        HistorySection(ride: Ride(id: "ewqqwe", userId: "ewqqeww", scooterId: "qweqeqwe", startDate: "qweeqweeqw", number: 23, v: 0, endDate: "qwewqeqe", distance: 89.3, duration: "00:09:09", totalPrice: 89.09, startAddress: "Strada Micu Kelin, 6", endAddress: "Piata Unirii", path: [[], []]))
    }
}
