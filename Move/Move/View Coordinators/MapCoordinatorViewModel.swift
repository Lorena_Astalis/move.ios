//
//  MapCoordinatorViewModel.swift
//  Move
//
//  Created by Lorena Astalis on 18.05.2021.
//

import Foundation

enum Overlays {
    case scooters
    case unlockMethods
    case tripDetails
    case none
}

class MapCoordinatorViewModel: ObservableObject {
    @Published var locationManager = LocationManager()
    @Published var scooter: Scooter?
    @Published var booking: Booking?
    @Published var overlay: Overlays
    @Published var scooters: [Scooter]?
    @Published var scooterToUnlock: Scooter?
    
    init() {
        self.overlay = Overlays.tripDetails
        self.getCurrentBooking()
    }
    
    func getCurrentBooking() {
        API.getCurrentBooking({ result in
            switch result {
            case .success(let booking):
                self.overlay = Overlays.tripDetails
                self.getScooterById(id: booking.booking.scooterId)
                self.booking = booking
                break
            case .failure:
                self.overlay = Overlays.none
                break
            }
        })
    }
    
    func getScooterById(id: String) {
        API.getScooterById(scooterId: id, { result in
            switch result {
            case .success(let scooter):
                self.scooter = scooter.scooter
                break
            case .failure:
                break
            }
        })
    }
    
    func lockScooter() {
        guard let booking = self.booking else {
            return
        }
        API.lockUnlockScooter(bookingId: booking.booking.id, { result in
            switch result {
            case .success:
                print("Success")
                break
            case .failure(let error):
                showError(error: error.localizedDescription)
                break
            }
        })
    }
    
    func reset() {
        self.scooter = nil
        self.booking = nil
        self.overlay = .none
        self.scooters = nil
        self.scooterToUnlock = nil
    }
}
