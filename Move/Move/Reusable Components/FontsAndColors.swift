//
//  FontsAndColors.swift
//  Move
//
//  Created by Lorena Astalis on 08.04.2021.
//

import SwiftUI

extension Color {
    static var trueRed: Color {
        return Color("TrueRed")
    }
    
    static var raspberry: Color {
        return Color("Raspberry")
    }
    
    static var raspberryShadow: Color {
        return Color("RaspberryShadow")
    }
    
    static var cloudy: Color {
        return Color("Cloudy")
    }
    
    static var indigo: Color {
        return Color("Indigo")
    }
    
    static var gray: Color {
        return Color("Gray")
    }
}

extension Font {
    static var baiJamjureeBold16: Font {
        return Font
            .custom("BaiJamjuree-Bold", size: 16)
    }
    
    static var baiJamjureeBold18: Font {
        return Font
            .custom("BaiJamjuree-Bold", size: 18)
    }
    
    static var baiJamjureeBold20: Font {
        return Font
            .custom("BaiJamjuree-Bold", size: 20)
    }
    
    static var baiJamjureeBold32: Font {
        return Font
            .custom("BaiJamjuree-Bold", size: 32)
    }
    static var baiJamjureeBold44: Font {
        return Font
            .custom("BaiJamjuree-Bold", size: 44)
    }
    
    static var baiJamjureeRegular16: Font {
        return Font
            .custom("BaiJamjuree-Regular", size: 16)
    }
    
    static var baiJamjureeRegular12: Font {
        return Font
            .custom("BaiJamjuree-Regular", size: 12)
    }
    
    static var baiJamjureeMedium16: Font {
        return Font
            .custom("BaiJamjuree-Medium", size: 16)
    }
    
    static var baiJamjureeMedium12: Font {
        return Font
            .custom("BaiJamjuree-Medium", size: 12)
    }
    
    static var baiJamjureeMedium14: Font {
        return Font
            .custom("BaiJamjuree-Medium", size: 14)
    }
    
    static var baiJamjureeMedium20: Font {
        return Font
            .custom("BaiJamjuree-Medium", size: 20)
    }
    
    static var baiJamjureeSemiBold14: Font {
        return Font
            .custom("BaiJamjuree-SemiBold", size: 14)
    }
    
    static var baiJamjureeSemiBold17: Font {
        return Font
            .custom("BaiJamjuree-SemiBold", size: 17)
    }
    
    static var baiJamjureeSemiBold12: Font {
        return Font
            .custom("BaiJamjuree-SemiBold", size: 12)
    }
    
}
