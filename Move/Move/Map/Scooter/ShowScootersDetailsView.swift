//
//  ShowScootersDetailesView.swift
//  Move
//
//  Created by Lorena Astalis on 21.04.2021.
//

import SwiftUI

struct ShowScootersDetailsView: View {
    let scooters: [Scooter]
    let location: [Double]
    let onUnlock: (Scooter) -> Void
    
    var body: some View {
        GeometryReader { geometry in
            HStack(alignment: .center) {
                Spacer()
                ForEach(scooters, id: \.id) { scooter in
                    ScooterDetailsView(scooter: scooter, booking: nil, onPingScooter: {
                        API.pingScooter(scooterId: scooter.id, lat: location[0], long: location[1], callback: { result in
                            switch result {
                            case .success: break
                            case .failure(let error):
                                showError(error: error.localizedDescription)
                                break
                            }
                            
                        })
                    }, onUnlock: { scooter in
                        onUnlock(scooter)
                    })
                        .frame(width: geometry.size.width / 2 + 30)
                        .padding(30)
                }
                Spacer()
            }
        }.frame(height: 315)
    }
}

struct ShowScootersDetailesView_Previews: PreviewProvider {
    static var previews: some View {
        ShowScootersDetailsView(scooters:  [Scooter(id: "26451752fweu7ryfwu", battery: 89, location: Location(type: "Point", coordinates: [23.591423, 46.770439]), locked: true, internalId: "23412235", uniqueNumber: "1234")], location: [34.543, 54.58974], onUnlock: {_ in })
                    
    }
}
