//
//  HistoryViewModel.swift
//  Move
//
//  Created by Lorena Astalis on 05.05.2021.
//

import Foundation


class HistoryViewModel: ObservableObject {
    @Published var rides: [Ride]
    var start = 0
    var length = 10
    
    init() {
        self.rides = []
        API.getAllBookings(start: start, length: length) { result in
            switch result {
            case .success(let rides):
                self.rides = rides.bookings
                break
            case .failure(let error):
                showError(error: error.localizedDescription)
                break
            }
        }
    }
    
    func loadMore() {
        length += 10
        refresh()
    }
    
    func refresh() {
        API.getAllBookings(start: start, length: length) { result in
            switch result {
            case .success(let rides):
                self.rides = rides.bookings
                break
            case .failure(let error):
                showError(error: error.localizedDescription)
                break
            }
        }
    }
    
}

