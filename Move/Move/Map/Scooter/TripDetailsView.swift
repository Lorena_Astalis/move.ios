//
//  TripDetailesView.swift
//  Move
//
//  Created by Lorena Astalis on 04.05.2021.
//

import SwiftUI
import CoreLocation

struct TripDetailsView: View {
    @ObservedObject var viewModel: TripDetailsViewModel
    @State var offset: CGFloat = 500
    @State var waiting = false
    var timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    let onLock: () -> Void
    let onEndTrip: (BookingComplete) -> Void
    
    init(booking: Booking, onLock: @escaping () -> Void, onEndTrip: @escaping (BookingComplete) -> Void) {
        self.viewModel = TripDetailsViewModel(booking: booking)
        self.onLock = onLock
        self.onEndTrip = onEndTrip
    }
    
    var body: some View {
        ZStack {
            GeometryReader { geometry in
                if offset != 0 {
                card
                    .transition(.scale)
                    .gesture(
                        DragGesture()
                            .onChanged { gesture in
                                offset = gesture.location.y
                            }
                            
                            .onEnded { value in
                                if value.location.y > offset {
                                    offset = geometry.size.height - 310
                                } else {
                                    offset = 0
                                }
                            }
                    )
                } else {
                    ZStack {
                        Color.white
                            .ignoresSafeArea()
                        VStack {
                            StickyHeaderView(backImage: Image("down-arrow-glyph"), title: "Trip details", image: nil, color: Color.indigo, onBack: {
                                        offset = geometry.size.height - 310
                            })
                            
                            Spacer()
                            expandedContent
                            HStack(spacing: 20) {
                                StrokeButtonView(image: viewModel.scooter?.locked ?? false ? Image("unlocked-lock-glyph"): Image("lock-glyph"), text: viewModel.scooter?.locked ?? false ? "Unlock": "Lock", color: .raspberry, pressed: {
                                    viewModel.onLockUnlock()
                                })
                                ButtonView(text: "End ride", activateButton: true, callback: {
                                    waiting = true
                                    viewModel.endRide { result in
                                            switch result {
                                            case .success(let result):
                                                viewModel.endRide = true
//                                                for path in result.booking.path {
//                                                    let location = CLLocationCoordinate2D(latitude: path[1], longitude: path[0])
//                                                    coordinates.append(location)
//                                                }
                                                onEndTrip(result)
                                                break
                                            case .failure(let error):
                                                waiting = false
                                                showError(error: error.localizedDescription)
                                                break
                                            }
                                    }
                                }, image: nil, color: Color.white, waiting: waiting)
                            }
                            .padding(.horizontal, 24)
                            .padding(.bottom, 12)
                        }
                    }
                    .gesture(
                        DragGesture()
                            .onChanged { gesture in
                                if gesture.location.y > offset {
                                    offset = gesture.location.y
                                }
                            }
                            
                            .onEnded { value in
                                    offset = geometry.size.height - 310
                            }
                    )
                    .animation(.some(Animation.easeInOut))
                    .offset(y: offset)
                }
            }
        }
        .transition(.slide)
    }
    
    var card: some View {
        GeometryReader { geometry in
            VStack {
                Spacer()
                ZStack(alignment: .top) {
                    RoundedRectangle(cornerRadius: 29)
                            .fill(Color.white)
                    Image("pink-dash-glyph")
                    content
                }
                .offset(y: offset)
                .onAppear {
                    offset = geometry.size.height - 310
                }
            }
        }
    }
    
    var content: some View {
        VStack {
            Text("Trip detailes")
                .font(.baiJamjureeSemiBold17)
                .foregroundColor(.indigo)
                .padding(24)
            HStack {
                BatteryView(batteryLevel: viewModel.scooter?.battery ?? 0, expanded: false)
                    .padding(.horizontal, 24)
                Spacer()
            }
            HStack {
                VStack(alignment: .leading, spacing: 24) {
                    VStack(alignment: .leading, spacing: 0) {
                        section(image: Image("clock-glyph"), text: "Travel time")
                        HStack(alignment: .bottom) {
                            Text(computeTimeShortFormat())
                                .font(.baiJamjureeBold32)
                                .foregroundColor(.indigo)
                                .onReceive(timer) { _ in
                                    viewModel.time += 1
                                }
                            Text("min")
                                .font(.baiJamjureeBold16)
                                .foregroundColor(.indigo)
                                .padding(.bottom, 5)
                        }
                    }
                    StrokeButtonView(image: viewModel.scooter?.locked ?? false ? Image("unlocked-lock-glyph"): Image("lock-glyph"), text: viewModel.scooter?.locked ?? false ? "Unlock": "Lock", color: .raspberry, pressed: {
                        viewModel.onLockUnlock()
                    })
                }
                .padding(.horizontal, 24)
                Spacer()
                VStack(alignment: .leading, spacing: 24) {
                    VStack(alignment: .leading, spacing: 0) {
                        section(image: Image("map-glyph"), text: "Distance")
                        HStack(alignment: .bottom) {
                            Text(String(format: "%.1f", viewModel.totalDistance))
                                .font(.baiJamjureeBold32)
                                .foregroundColor(.indigo)
                            Text("km")
                                .font(.baiJamjureeBold16)
                                .foregroundColor(.indigo)
                                .padding(.bottom, 5)
                        }
                    }
                    ButtonView(text: "End ride", activateButton: true, callback: {
                        waiting = true
                        viewModel.endRide { result in
                            switch result {
                            case .success(let result):
                                viewModel.endRide = true
                                onEndTrip(result)
                                break
                            case .failure(let error):
                                waiting = false
                                showError(error: error.localizedDescription)
                                break
                            }
                        }
                    }, image: nil, color: Color.white, waiting: waiting)
                }
                .padding(24)
            }
        }
    }
    
    var expandedContent: some View {
        GeometryReader { geometry in
            VStack {
                RoundedRectangle(cornerRadius: 29)
                    .strokeBorder(Color.cloudy)
                    .overlay(BatteryView(batteryLevel: viewModel.scooter?.battery ?? 0, expanded: true))
                    .padding(.bottom, 24)
                RoundedRectangle(cornerRadius: 29)
                    .strokeBorder(Color.cloudy)
                    .overlay(
                        VStack {
                            section(image: Image("clock-glyph"), text: "Travel time")
                            Text(computeTimeLongFormat())
                                .font(.baiJamjureeBold44)
                                .foregroundColor(.indigo)
                                .multilineTextAlignment(.center)
                                .onReceive(timer) { _ in
                                    viewModel.time += 1
                                }
                                .frame(width: 200)
                    })
                    .padding(.bottom, 24)
                RoundedRectangle(cornerRadius: 29)
                    .strokeBorder(Color.cloudy)
                    .overlay(VStack {
                        section(image: Image("map-glyph"), text: "Distance")
                        Text(String(format: "%.1f", viewModel.totalDistance))
                            .font(.baiJamjureeBold44)
                            .foregroundColor(.indigo)
                        Text("km")
                            .font(.baiJamjureeMedium16)
                    })
                    .padding(.bottom, 24)
            }
            .padding(.horizontal, 24)
        }
       
    }
    
    func section(image: Image, text: String) -> some View {
        HStack {
            image
            Text(text)
                .font(.baiJamjureeMedium16)
                .foregroundColor(.cloudy)
        }
    }
    
    func computeTimeShortFormat() -> String {
        return String(format: "%02d:%02d", viewModel.time / 3600, viewModel.time / 60)
    }
    
    func computeTimeLongFormat() -> String {
        return String(format: "%02d:%02d:%02d ", viewModel.time / 3600, (viewModel.time / 60) % 60, viewModel.time % 60)
    }
}

//struct TripDetailsView_Previews: PreviewProvider {
//    static var previews: some View {
//        TripDetailsView(booking: Scooter(id: "323425532", battery: 56, location: Location(type: "Point", coordinates: [12.4353, 54.543345]), locked: false, internalId: "2342332432", uniqueNumber: "1234"), onLock: {}, onEndTrip: {_ in})
//    }
//}
