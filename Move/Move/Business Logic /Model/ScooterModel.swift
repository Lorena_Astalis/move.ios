//
//  ScooterModel.swift
//  Move
//
//  Created by Lorena Astalis on 19.04.2021.
//

import Foundation

struct Location: Codable {
    let type: String
    let coordinates: [Double]
    
    enum CodingKeys: String, CodingKey {
        case type = "type"
        case coordinates = "coordinates"
    }
}

struct Scooter: Codable, Identifiable {
    let id: String
    var battery: Int
    let location: Location
    let locked: Bool
    let internalId: String
    let uniqueNumber: String
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case battery = "battery"
        case location = "location"
        case locked = "locked"
        case internalId = "internalId"
        case uniqueNumber = "uniqueNumber"
    }
}

struct ScooterResult: Codable {
    let scooters: [Scooter]
}

struct ScooterResponse: Codable {
    let scooter: Scooter
    
    enum CodingKeys: String, CodingKey {
        case scooter = "scooter"
    }
}

