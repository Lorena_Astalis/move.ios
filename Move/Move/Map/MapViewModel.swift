//
//  MapViewModel.swift
//  Move
//
//  Created by Lorena Astalis on 19.04.2021.
//

import Foundation
import CoreLocation
import Combine

class MapViewModel: ObservableObject {
    @Published var scooters: [Scooter]
    var currentLocation: CLLocationCoordinate2D?
    var locationManager: LocationManager
    var anyCancellable: AnyCancellable? = nil
    
    init(locationManager: LocationManager) {
        scooters = []
        self.locationManager = locationManager
        anyCancellable = locationManager.objectWillChange.sink { [weak self] in
            guard let self = self else {
                return
            }
            let firstLocationUpdate = self.currentLocation == nil
            self.currentLocation = self.locationManager.location?.coordinate
            
            if firstLocationUpdate {
                self.loadData()
            }
        }
    }
    
    func refreshScooters() {
        guard let currentLocation = self.currentLocation else {
            return
        }
        print(currentLocation)
        API.getScooters(long: currentLocation.longitude, lat: currentLocation.latitude) { (result) in
            switch result {
            case .success(let scooters):
                self.scooters = scooters.scooters
            case .failure:
                break
            }
        }
    }
    
    func loadData() {
        self.refreshScooters()
        DispatchQueue.main.asyncAfter(deadline: .now() + 30) {
            self.loadData()
        }
    }
}
